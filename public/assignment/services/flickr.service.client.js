/**
 * Created by Nicholas on 2/18/17.
 */
(function(){
    angular
        .module("WebAppMaker")
        .factory("FlickrService", FlickrService);


    function FlickrService($http) {

        var key = "a7fe473804820dc09fc5ac7419f057cd";
        var secret = "6a9f151c055b9f5c";
        var urlBase = "https://api.flickr.com/services/rest/?method=flickr.photos.search&format=json&api_key=API_KEY&text=TEXT";

        var api = {
            "searchPhotos" : searchPhotos
        };
        return api;

        function searchPhotos(searchTerm) {
            var url = urlBase.replace("API_KEY", key).replace("TEXT", searchTerm);
            return $http.get(url);
        }

    }

})();