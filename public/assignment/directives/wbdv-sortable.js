/**
 * Created by Nicholas on 2/16/17.
 */

(function () {
    angular
        .module('WebAppMaker')
        .directive('wbdvSortable', ['$routeParams' , sortableDir]);

    function sortableDir() {

        var initial = -1;
        var final = -1;

        function linkFunc(scope, element, attributes) {
            element.sortable({axis: 'y',
                start: function(event, ui) {
                    initial = ui.item.index();
                },
                stop: function(event, ui) {
                    final = ui.item.index();
                    scope.SortingController.sortWidgets(initial, final);
                }
            });
        }

        return {
            scope: {},
            link: linkFunc,
            controller: SortingController,
            controllerAs: "SortingController"
        };
    }

    function SortingController($routeParams, WidgetService) {
        var vm = this;
        vm.pageId = $routeParams["pid"];

        vm.sortWidgets = sortWidgets;

        function sortWidgets(initial, final) {
            WidgetService.sortWidgets(vm.pageId, initial, final);
        }

    }



})();