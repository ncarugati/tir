/**
 * Created by Nicholas on 1/30/17.
 */

(function() {
    angular
        .module("WebAppMaker")
        .controller("PageListController", PageListController)
        .controller("NewPageController", NewPageController)
        .controller("EditPageController", EditPageController);

    function PageListController($routeParams, PageService) {
        var vm = this;
        vm.userId = $routeParams["uid"];
        vm.websiteId = $routeParams["wid"];
        function init() {
            var pages = PageService.findPageByWebsiteId(vm.websiteId);
            pages.success(function(response) {
               if(response) {
                   vm.pages = response;
               }
            });
        }
        init();
    }

    function NewPageController($location, $routeParams, PageService) {
        var vm = this;
        vm.newPage = newPage;
        vm.userId = $routeParams["uid"];
        vm.websiteId = $routeParams["wid"];

        function newPage(name, desc) {
            if(!name) {
                vm.error = "No name provided.";
                return;
            }
            if(!desc) {
                vm.error = "No title provided.";
                return;
            }
            var id = new Date().getTime();
            var pg = { _id: id, name: name, websiteId: vm.wid, title: desc };
            var makePage = PageService.createPage(vm.websiteId, pg);
            makePage.success(function(response) {
                if(response) {
                    $location.url("/user/" + vm.userId + "/website/" + vm.websiteId + "/page");
                }
            });
        }
    }


    function EditPageController($location, $routeParams, PageService) {
        var vm = this;
        vm.websiteId = $routeParams["wid"];
        vm.userId = $routeParams["uid"];
        vm.pageId = $routeParams["pid"];
        var currPage = PageService.findPageById(vm.pageId);
        currPage.success(function(response) {
            if(response) {
                vm.currPage = response;
            }
        });

        vm.updatePage = updatePage;
        vm.deletePage = deletePage;

        function updatePage() {
            var pageUpdate = PageService.updatePage(vm.pageId, vm.currPage);
            pageUpdate.success(function(response) {
               if(response) {
                   vm.message = "Page successfully edited."; //TODO: Pass data through other views
                   $location.url("/user/" + vm.userId + "/website/" + vm.websiteId + "/page");
               }
            });
        }

        function deletePage() {
            var delPage = PageService.deletePage(vm.pageId);
            delPage.success(function(response) {
               if(response) {
                   $location.url("/user/" + vm.userId + "/website/" + vm.websiteId + "/page");
               }
            });
        }
    }

})();