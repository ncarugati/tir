/**
 * Created by Nicholas on 1/30/17.
 */

(function() {
    angular
        .module("WebAppMaker")
        .controller("WebsiteListController", WebsiteListController)
        .controller("NewWebsiteController", NewWebsiteController)
        .controller("EditWebsiteController", EditWebsiteController);


    function WebsiteListController($routeParams, WebsiteService) {
        var vm = this;
        vm.userId = $routeParams["uid"];
        function init() {
            var _websites = WebsiteService.findWebsitesByUser(vm.userId);
            _websites.success(function(response) {
                if(response) {
                    vm.websites = response;
                }
            });
        }
        init();
    }

    function NewWebsiteController($location, $routeParams, WebsiteService) {
        var vm = this;
        vm.newWebsite = newWebsite;
        vm.userId = $routeParams["uid"];

        function newWebsite(name, desc) {
            if(!name) {
                vm.error = "No name provided.";
                return;
            }
            var wId = new Date().getTime();
            var newWebsite = { _id: wId, name: name, developerId: vm.userId, description: desc };
            var createWeb = WebsiteService.createWebsite(vm.userId, newWebsite);
            createWeb.success(function(response) {
               if(response) {
                   $location.url("/user/" + vm.userId + "/website");
               }
            });
        }

        function init() {
            var _websites = WebsiteService.findWebsitesByUser(vm.userId);
            _websites.success(function(response) {
                if(response) {
                    vm.websites = response;
                }
            });
        }
        init();
    }

    function EditWebsiteController($location, $routeParams, WebsiteService) {
        var vm = this;
        vm.websiteId = $routeParams["wid"];
        vm.userId = $routeParams["uid"];
        var currWeb = WebsiteService.findWebsiteById(vm.websiteId);
        currWeb.success(function(response) {
           if(response) {
               vm.currWebsite = response;
           }
        });

        vm.updateWebsite = updateWebsite;
        vm.deleteWebsite = deleteWebsite;

        function updateWebsite(website) {
            var updateWeb = WebsiteService.updateWebsite(vm.websiteId, website);
            updateWeb.success(function(response) {
               if(response) {
                   vm.message = "Website successfully updated."; //TODO: Pass data through other views
                   $location.url("/user/" + vm.userId + "/website");
               }
            });
        }

        function deleteWebsite() {
            var delWeb = WebsiteService.deleteWebsite(vm.websiteId);
            delWeb.success(function(response) {
                if(response) {
                    $location.url("/user/" + vm.userId + "/website");
                }
            });
        }

        function init() {
            var _websites = WebsiteService.findWebsitesByUser(vm.userId);
            _websites.success(function(response) {
                if(response) {
                    vm.websites = response;
                }
            });
        }
        init();

    }

})();