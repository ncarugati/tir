/**
 * Created by Nicholas on 2/18/17.
 */


(function() {
    angular
        .module("WebAppMaker")
        .controller("FlickrImageSearchController", FlickrImageSearchController);

    function FlickrImageSearchController($location, $routeParams, WidgetService, FlickrService) {
        var vm = this;

        vm.userId = $routeParams["uid"];
        vm.websiteId = $routeParams["wid"];
        vm.pageId = $routeParams["pid"];
        vm.widgetId = $routeParams["wgid"];

        vm.searchPhotos = searchPhotos;
        vm.selectPhoto = selectPhoto;

        function searchPhotos(searchTerm) {
            FlickrService
                .searchPhotos(searchTerm)
                .then(function(response) {
                    data = response.data.replace("jsonFlickrApi(","");
                    data = data.substring(0,data.length - 1);
                    data = JSON.parse(data);
                    vm.photos = data.photos;
                });
        }

        function selectPhoto(photo) {
            var url = "https://farm" + photo.farm + ".staticflickr.com/" + photo.server;
            url += "/" + photo.id + "_" + photo.secret + "_b.jpg";
            var widgt_new = { "type": "IMAGE", "_page": vm.pageId, "width": "100%",
                "url": url};
            WidgetService
                .updateWidget(vm.widgetId, widgt_new)
                .then(function(response) {
                    var url = "/user/" + vm.userId + "/website/" + vm.websiteId +
                        "/page/" + vm.pageId + "/widget/" + vm.widgetId;
                    $location.url(url);
                });
        }
    }

})();