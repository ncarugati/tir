/**
 * Created by Nicholas on 1/30/17.
 */

(function() {
    angular
        .module("WebAppMaker")
        .controller("WidgetListController", WidgetListController)
        .controller("NewWidgetController", NewWidgetController)
        .controller("EditWidgetController", EditWidgetController);


    function WidgetListController($sce, $routeParams, WidgetService) {
        var vm = this;
        vm.userId = $routeParams["uid"];
        vm.websiteId = $routeParams["wid"];
        vm.pageId = $routeParams["pid"];
        vm.renderHTML = renderHTML;
        vm.renderYoutube = renderYoutube;

        function renderHTML(widget) {
            return $sce.trustAsHtml(widget.text);
        }

        function renderYoutube(widget) {
            var urlParts = widget.url.split("/");
            var id = urlParts[urlParts.length - 1];
            var url = "https://www.youtube.com/embed/" + id;
            return $sce.trustAsResourceUrl(url);
        }

        function init() {
            var widgets = WidgetService.findWidgetsByPageId(vm.pageId);
            widgets.success(function(response) {
                if(response) {
                    vm.widgets = response;
                }
            });
        }
        init();
    }

    function NewWidgetController($location, $routeParams, WidgetService) {
        var vm = this;

        vm.newWidget = newWidget;

        vm.userId = $routeParams["uid"];
        vm.websiteId = $routeParams["wid"];
        vm.pageId = $routeParams["pid"];

        function newWidget(type) {
            constructWidgetArray(type);
            var widgetCreation = WidgetService.createWidget(vm.pageId, vm.widgt_new);
            widgetCreation.success(function(response) {
                if(response) {
                    var id = response._id;
                    $location.url("/user/" + vm.userId +
                        "/website/" + vm.websiteId + "/page/" + vm.pageId + "/widget/" + id);
                }
            });
        }

        function constructWidgetArray(type) {
            switch(type) {
                case "HTML":
                    vm.widgt_new = { "type": "HTML", "text": "HTML Text Here"};
                    break;
                case "YOUTUBE":
                    vm.widgt_new = { "type": "YOUTUBE", "width": "100%",
                        "url": "" };
                    break;
                case "IMAGE":
                    vm.widgt_new = { "type": "IMAGE", "width": "100%", "url": ""};
                    break;
                case "HEADER":
                    vm.widgt_new = { "type": "HEADER", "text": "Header text here"};
                    break;
                case "TEXT":
                    vm.widgt_new = { "type": "TEXT", "text": "Header text here", "placeholder": "",
                        "formatted": false};
                    break;
                default:
                    vm.widgt_new = { "type": "TEXT", "text": "Header text here", "placeholder": "", "formatted": false};
                    break;
            }
        }

        function init() {
            vm.widgt_new = { "type": "NULL" };
        }
        init();

    }

    function EditWidgetController($location, $routeParams, WidgetService) {
        var vm = this;

        vm.updateWidget = updateWidget;
        vm.deleteWidget = deleteWidget;

        vm.userId = $routeParams["uid"];
        vm.websiteId = $routeParams["wid"];
        vm.pageId = $routeParams["pid"];
        vm.widgetId = $routeParams["wgid"];
        var widgetById = WidgetService.findWidgetById(vm.widgetId);
        widgetById.success(function(response) {
           if(response) {
               vm.widgt = response;
           }
        });

        function deleteWidget() {
            var widgetDelete = WidgetService.deleteWidget(vm.widgetId);
            widgetDelete.success(function(response) {
                if(response) {
                    $location.url("/user/" + vm.userId +
                        "/website/" + vm.websiteId + "/page/" + vm.pageId + "/widget");
                }
            });
        }

        function updateWidget() {
            var widgetUpdate = WidgetService.updateWidget(vm.widgetId, vm.widgt);
            widgetUpdate.success(function(response) {
                if(response) {
                    vm.message = "Widget successfully updated." //TODO: Pass data through other views
                    $location.url("/user/" + vm.userId +
                        "/website/" + vm.websiteId + "/page/" + vm.pageId + "/widget");
                }
            });
        }
    }

})();