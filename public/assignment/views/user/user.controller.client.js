/**
 * Created by Nicholas on 1/30/17.
 */

(function() {
    angular
        .module("WebAppMaker")
        .controller("LoginController", LoginController)
        .controller("RegisterController", RegisterController)
        .controller("ProfileController", ProfileController);

    function LoginController($location, $rootScope, UserService) {
        var vm = this;
        vm.login = login;

        function login(user) {
            UserService.login(user)
                .success(function(response) {
                    if(!response) {
                        vm.error = "Unable to login.";
                        return;
                    }
                    var loginUser = response;
                    $rootScope.currentUser = user;
                    if(loginUser._id) {
                        $location.url("/user/" + loginUser._id);
                    } else {
                        vm.error = "Unable to login.";
                    }
                })
                .error(function(data, status) {

                    if(status == 401) {
                        vm.error = "Invalid username or password.";
                    } else {
                        vm.error = "Internal server error please try again later.";
                        console.log(data);
                    }
                });
        }
    }

    function RegisterController($location, $rootScope, UserService) {
        var vm = this;
        vm.register = register;

        function register(username, password, pwdVer) {
            if(!username) {
                vm.error = "No username entered";
                return;
            }
            if(!password) {
                vm.error = "No password entered";
                return;
            }
            if(userExists(username)) {
                vm.error = "This user currently exists";
            }
            if(password === pwdVer) {
                var newUser = {username: username, password: password,
                    firstName: '', lastName: '', email: '' };
                var useCreate = UserService.createUser(newUser);
                useCreate.success(function(response) {
                    if(response) {
                        $rootScope.currentUser = response;
                        var id = response._id;
                        $location.url("/user/" + id);
                    }
                });
            } else {
                vm.error = "Passwords do not match.";
            }

        }

        function userExists(username) {
            var user = UserService.findUserByUsername(username);
            user.success(function(response) {
                if(response) {
                    return user !== null;
                }
            });
            return false;
        }

    }

    function ProfileController($routeParams, $rootScope, UserService) {
        var vm = this;
        var userId = $routeParams["uid"];
        vm.updateUserInfo = updateUserInfo;
        vm.logout = logout;

        function init() {
            var _user = UserService.findUserById(userId);
            _user.success(function(response) {
                vm.user = response;
            });
        }
        init();

        function updateUserInfo() {
            var isUpdated = UserService.updateUser(vm.user._id, vm.user);
            isUpdated.success(function(response) {
                if(!response) {
                    vm.error = "Failed to update profile.";
                    return;
                } else {
                    vm.message = "Successfully updated profile.";
                }
            });
        }

        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/");
                });
        }


    }

})();