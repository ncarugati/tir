/**
 * Created by Nicholas on 3/28/17.
 */
(function() {
    angular
        .module("TirApp")
        .controller("AdminController", AdminController);

    function AdminController($location, $routeParams, $route, $rootScope, UserService, AdminService) {
        var vm = this;
        vm.logout = logout;
        var userId = $routeParams["uid"];
        vm.currentUserView = null;

        var NEW_USER_TEMPLATE = {
            username: "",
            password: "",
            firstName: "",
            lastName: "",
            email: "",
            summary: "",
            gender: "Male",
            phone: "",
            age: new Date(),
            rights: false,
            picture: ""
        };

        vm.setUserView = setUserView;
        vm.removeUser = removeUser;
        vm.updateUserInfo = updateUserInfo;
        vm.setRights = setRights;
        vm.createUser = createUser;


        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/login");
                });
        }

        function removeUser(user) {
            UserService.removeUser(user._id)
                .then(function(response) {
                    if(response) {
                        getAllUsers();
                        $route.reload();
                    }
                })
        }

        function updateUserInfo(user) {
            UserService.updateUser(user._id, user, false)
                .then(function(response) {
                   if(response) {
                       getAllUsers();
                       $route.reload();
                   }
                });
        }

        function setRights(user, bool) {
            AdminService.setRights(user, bool)
                .then(function(response) {
                   if(response) {
                       getAllUsers();
                       $route.reload();
                   }
                });
        }

        function setUserView(user) {
            if(!user) {
                vm.currentUserView = NEW_USER_TEMPLATE;
                return;
            }
            vm.currentUserView = user;
            vm.currentUserView.age = new Date(vm.user.age);
        }


        function createUser(user, password_ver, email_ver) {
            if(user.password === password_ver) {
                if(user.email === email_ver) {
                    var query = AdminService.createUser(user);
                    query.then(function(response) {
                        if(response) {
                            $route.reload();
                        } else {
                            vm.error = "No response from server - Try again later.";
                        }
                    });
                } else {
                    vm.error = "Emails do not match.";
                }
            } else {
                vm.error = "Passwords do not match.";
            }
        }

        function getAllUsers() {
            AdminService.getAllUsers()
                .then(function(response) {
                   vm.userList = response.data;
                   getAllArticles();
                });
        }

        function getAllArticles() {
            AdminService.getAllArticles()
                .then(function(response) {
                    if(response) {
                        vm.artDataSize = response.data.length;
                        getAllComments();
                    }
                });
        }

        function getAllComments() {
            AdminService.getAllComments()
                .then(function(response) {
                    vm.commDataSize = response.data.length;
                });
        }

        function init() {
            var _user = UserService.findUserById(userId);
            _user.success(function(response) {
                vm.user = response;
                getAllUsers();
            });
        }
        init();



    }

})();
