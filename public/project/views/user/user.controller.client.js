/**
 * Created by Nicholas on 3/28/17.
 */
(function() {
    angular
        .module("TirApp")
        .controller("LaunchController", LaunchController)
        .controller("LoginController", LoginController)
        .controller("RegisterController", RegisterController)
        .controller("ProfileController", ProfileController)
        .controller("ProfileEditController", ProfileEditController)
        .controller("FeedController", FeedController);

    function LoginController($location, $rootScope, UserService) {
        var vm = this;
        vm.login = login;

        function login(user) {
            if(!user) {
                vm.error = "Please enter a username and password.";
                return;
            }
            if(!user.username) {
                vm.error = "Please enter a username.";
                return;
            }
            if(!user.password) {
                vm.error = "Please enter a password.";
                return;
            }
            UserService.login(user)
                .success(function(response) {
                    if(!response) {
                        vm.error = "Unable to login.";
                        return;
                    }
                    var loginUser = response;
                    $rootScope.currentUser = user;
                    if(loginUser._id) {
                        $location.url("/user/" + loginUser._id);
                    } else {
                        vm.error = "Unable to login.";
                    }
                })
                .error(function(data, status) {
                    if(status == 401) {
                        vm.error = "Invalid username or password.";
                    } else {
                        vm.error = "Internal server error please try again later.";
                    }
                });
        }
    }

    function RegisterController($location, $rootScope, UserService) {
        var vm = this;
        vm.register = register;

        function register(username, password, pwdVer, email, verEmail) {
            if(!username) {
                vm.error = "No username entered.";
                return;
            }
            if(!password) {
                vm.error = "No password entered.";
                return;
            }
            if(!email) {
                vm.error = "No email entered.";
                return;
            }
            if(userExists(username)) {
                vm.error = "This user currently exists.";
            }
            if(password === pwdVer) {
                if(email == verEmail) {
                    var newUser = {
                        username: username, password: password,
                        firstName: '', lastName: '', email: email };
                    var useCreate = UserService.createUser(newUser);
                    useCreate.success(function(response) {
                        if(response) {
                            $rootScope.currentUser = response;
                            var id = response._id;
                            $location.url("/user/" + id);
                        }
                    });
                } else {
                    vm.error = "Email addresses do not match."
                }
            } else {
                vm.error = "Passwords do not match.";
            }

        }

        function userExists(username) {
            var user = UserService.findUserByUsername(username);
            user.success(function (response) {
                if (response) {
                    return user !== null;
                }
            });
            return false;
        }

    }

    function ProfileController($routeParams, $rootScope, $location, $route,
                               UserService, FeedService, MediaService, CommentService) {
        var vm = this;
        var userId = $routeParams["uid"];
        vm.logout = logout;
        vm.printName = printName;
        vm.calculateAge = calculateAge;
        vm.removeFromProfile = removeFromProfile;
        vm.isProfileOwner = isProfileOwner;
        vm.follow = follow;
        vm.unfollow = unfollow;
        vm.isFollowing = isFollowing;
        vm.createComment = createComment;
        vm.determinePostControls = determinePostControls;
        vm.determineFollowStatus = determineFollowStatus;
        vm.toReadableDate = toReadableDate;
        vm.shareToProfile = shareToProfile;
        vm.prepareShare = prepareShare;
        vm.selectedArticle = null;


        function follow() {
            if(isFollowing()) {
                return;
            }
            var query = UserService.follow($rootScope.currentUser._id, vm.user);
            query.then(function(response) {
                if(response) {
                    UserService.gainFollower(vm.user._id, $rootScope.currentUser)
                        .then(function(response) {
                            if(response) {
                                vm.fStatus = true;
                                init();
                            }
                        });
                }
            });
        }

        function unfollow() {
            if(!isFollowing()) {
                return;
            }
            var query = UserService.unfollow($rootScope.currentUser._id, vm.user);
            query.then(function(response) {
                if(response) {
                    UserService.loseFollower(vm.user._id, $rootScope.currentUser)
                        .then(function(response) {
                            if(response) {
                                vm.fStatus = false;
                                init();
                            }
                        });
                }
            });
        }

        function toReadableDate(dateString) {
            return new Date(dateString).toDateString();
        }

        function isFollowing() {
            if(!$rootScope.currentUser || !vm.user) {
                return false;
            }
            var followingTuple = $rootScope.currentUser.following;
            for(var i = 0; i < followingTuple.length; i++) {
                if(vm.user._id === followingTuple[i].uid) {
                    return true;
                }
            }
            return false;
        }

        function determineFollowStatus() {
            if(isFollowing()) {
                return true;
            } else if(!isFollowing()) {
                return false;
            }
            return false;
        }

        function determinePostControls(media) {
            if(!$rootScope.currentUser) {
                return false;
            }
            if($rootScope.currentUser.rights) {
                return true;
            } else if(isProfileOwner()) {
                return true;
            } else if(media.posterId === $rootScope.currentUser._id) {
                return true;
            }
            return false;
        }

        function isProfileOwner() {
            if(!$rootScope.currentUser || !vm.user) {
                return false;
            }
            return $rootScope.currentUser._id === vm.user._id;
        }

        function fetchStories() {
            var query = MediaService.findAllMediaForProfile(userId);
            query.then(function(response) {
                if(response) {
                    vm.mediaTuple = response.data;
                    vm.mediaTuple.sort(function(i, j) {
                        return new Date(j.postDate) - new Date(i.postDate);
                    });
                    refreshCurrentUser();
                }
            })
        }

        function refreshCurrentUser() {
            if($rootScope.currentUser) {
                var _user = UserService.findUserById($rootScope.currentUser._id);
                _user.success(function (response) {
                    $rootScope.currentUser = response;
                });
            }
        }

        function removeFromProfile(media) {
            var id = media._id;
            var query = MediaService.deleteMedia(id);
            query.then(function(response) {
                if(response) {
                    vm.message = "Article successfully removed";
                    $route.reload();
                } else {
                    vm.error = "Unable to remove article."
                }
            })
        }


        function createComment(text, mediaId) {
            var config = {
                "userId": $rootScope.currentUser._id,
                "poster": printName($rootScope.currentUser),
                "picture": $rootScope.currentUser.picture,
                "text" : text
            };
            var query = CommentService.createComment(mediaId, config);
            query.then(function(response) {
               if(response) {
                   $route.reload();
               }
            }, function(error) {
                vm.error = "Unable to add comment - Internal server error.";
            });
        }

        function init() {
            var _user = UserService.findUserById(userId);
            _user.success(function(response) {
                vm.user = response;
                var interestLoad = FeedService.findAllFeedsForUser(vm.user._id);
                interestLoad.success(function(subresponse) {
                    if(subresponse) {
                        vm.interests = subresponse;
                        fetchStories();
                    }
                });
            });
        }
        init();


        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/login");
                });
        }

        function calculateAge(user) {
            var birthday = new Date(user.age);
            var now = new Date();

            var years = (now.getFullYear() - birthday.getFullYear());

            if (now.getMonth() < birthday.getMonth() ||
                now.getMonth() == birthday.getMonth() &&
                now.getDate() < birthday.getDate()) {
                years--;
            }
            return "" + years;
        }

        function prepareShare(feed) {
            vm.selectedArticle = feed;
            getFollowers();
        }

        function getFollowers() {
            var query = UserService.findUserFollowers(vm.user._id);
            query.then(function(response) {
                if(response) {
                    vm.followerTuple = [];
                    for(var i = 0; i < response.data[0].followers.length; i++) {
                        vm.followerTuple.push(response.data[0].followers[i].username);
                    }
                    console.log(vm.followersTuple);
                }
                getFollowing();
            });
        }

        function getFollowing() {
            var query = UserService.findUserFollowing(vm.user._id);
            query.then(function(response) {
                if(response) {
                    vm.followingTuple = [];
                    for(var i = 0; i < response.data[0].following.length; i++) {
                        vm.followingTuple.push(response.data[0].following[i].username);
                    }
                    console.log(vm.followingTuple);
                }
            });
        }

        function shareToProfile(username, feed) {
            if(!username) {
                vm.error = "No username entered.";
            }
            console.log(feed.image);
            var nameDisplay = printName($rootScope.currentUser);

            var config = {
                "poster": nameDisplay,
                "posterId": $rootScope.currentUser._id,
                "author": feed.author,
                "title": feed.title,
                "description": feed.description,
                "url": feed.url,
                "image": feed.image,
                "published": feed.published,
                "postDate": new Date(Date.now())
            };

            var userQuery = UserService.findUserByUsername(username);
            userQuery.then(function(response) {
                var query = MediaService.createMedia(response.data._id, config);
                query.then(function(resultant) {
                    if(resultant) {
                        vm.message = "Successfully added to " + response.data.username + "'s profile."
                    } else {
                        vm.error = "Unable to add to profile - no server response.";
                    }
                });
            });
        }


        function printName(user) {
            if(!user) {
                return "";
            }
            if(user.firstName && user.lastName) {
                return user.firstName + " " + user.lastName;
            } else if(user.firstName) {
                return user.firstName;
            } else if(user.lastName) {
                return user.lastName;
            }
            return user.username;
        }

    }

    function LaunchController($routeParams, $location, UserService) {
        var vm = this;
    }


    function ProfileEditController($routeParams, $rootScope, $location, UserService) {
        var vm = this;
        var userId = $routeParams["uid"];
        vm.updateUserInfo = updateUserInfo;
        vm.logout = logout;
        vm.deleteAccount = deleteAccount;

        function init() {
            var _user = UserService.findUserById(userId);
            _user.success(function(response) {
                vm.user = response;
                vm.user.age = new Date(vm.user.age);
            });
        }
        init();

        function updateUserInfo() {
            if(vm.password) {
                if(!vm.new_password || !vm.verify) {
                    vm.error = "You must enter a new password and verify it.";
                    return;
                }
                if(vm.new_password === vm.verify) {
                    vm.user.password = vm.password;
                    update(vm.verify);
                } else {
                    vm.error = "New passwords do not match.";
                }
            } else {
                update(null);
            }
        }

        function update(newPass) {
            var isUpdated = UserService.updateUser(vm.user._id, vm.user, newPass);
            isUpdated.success(function(response) {
                if(!response) {
                    vm.error = "Failed to update profile.";
                } else {
                    vm.message = "Successfully updated profile.";
                }
            }).error(function(data, status) {
                vm.error = "Current password input valid.";
            });
        }

        function deleteAccount() {
            UserService.logout()
                .success(function(response) {
                    if(response) {
                        $rootScope.currentUser = null;
                        $location.url("/login");
                        UserService.deleteUser(vm.user._id)
                            .then(function(response) {

                            });
                    }
                });
        }

        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/login");
                });
        }


    }


    function FeedController($routeParams, $rootScope, $route, $location, UserService, FeedService, MediaService) {
        var vm = this;
        var userId = $routeParams["uid"];

        vm.generateFeed = generateFeed;
        vm.renderHTML = renderHTML;
        vm.logout = logout;
        vm.addTopic = addTopic;
        vm.addToProfile = addToProfile;
        vm.fetchFeedData = fetchFeedData;
        vm.removeFeed = removeFeed;
        vm.shareToProfile = shareToProfile;
        vm.prepareShare = prepareShare;
        vm.toReadableDate = toReadableDate;
        vm.setSelection = setSelection;
        vm.selectedArticle = null;
        vm.tabSelection = false;


        function addToProfile(feed) {
            var nameDisplay = getNameRepresentation();
            var config = {
                "poster": nameDisplay,
                "posterId": vm.user._id,
                "author": feed.author,
                "title": feed.title,
                "description": feed.description,
                "url": feed.url,
                "image": feed.urlToImage,
                "published": feed.publishedAt,
                "postDate": new Date(Date.now())
            };

            var query = MediaService.createMedia(userId, config);
            query.then(function(resultant) {
                if(resultant) {
                    vm.message = "Successfully added to profile."
                } else {
                    vm.error = "Unable to add to profile - no server response.";
                }
                console.log(resultant);
            });

        }

        function toReadableDate(dateString) {
            return new Date(dateString).toDateString();
        }

        function prepareShare(feed) {
            vm.selectedArticle = feed;
            getFollowers();
        }

        function getNameRepresentation() {
            if(vm.user.firstName && vm.user.lastName) {
                return vm.user.firstName + " " + vm.user.lastName;
            }  else if(vm.user.firstName) {
                return vm.user.firstName;
            } else if(vm.user.lastName) {
                return vm.user.lastName;
            } else {
                return vm.user.username;
            }
        }


        function getFollowers() {
            var query = UserService.findUserFollowers(vm.user._id);
            query.then(function(response) {
                if(response) {
                    vm.followerTuple = [];
                    for(var i = 0; i < response.data[0].followers.length; i++) {
                        vm.followerTuple.push(response.data[0].followers[i].username);
                    }
                    console.log(vm.followersTuple);
                }
                getFollowing();
            });
        }

        function getFollowing() {
            var query = UserService.findUserFollowing(vm.user._id);
            query.then(function(response) {
                if(response) {
                    vm.followingTuple = [];
                    for(var i = 0; i < response.data[0].following.length; i++) {
                        vm.followingTuple.push(response.data[0].following[i].username);
                    }
                    console.log(vm.followingTuple);
                }
            });
        }

        function shareToProfile(username, feed) {
            if(!username) {
                vm.error = "No username entered.";
            }
            var nameDisplay = getNameRepresentation();

            var config = {
                "poster": nameDisplay,
                "posterId": vm.user._id,
                "author": feed.author,
                "title": feed.title,
                "description": feed.description,
                "url": feed.url,
                "image": feed.urlToImage,
                "published": feed.publishedAt,
                "postDate": new Date(Date.now())
            };

            var userQuery = UserService.findUserByUsername(username);
            userQuery.then(function(response) {
                if(!response.data) {
                    vm.error = "User does not exist";
                    return;
                }
                var query = MediaService.createMedia(response.data._id, config);
                query.then(function(resultant) {
                    if(resultant) {
                        vm.message = "Successfully added to " + response.data.username + "'s profile."
                    } else {
                        vm.error = "Unable to add to profile - no server response.";
                    }
                });
            });
        }

        function generateFeed() {
            var qResult = FeedService.queryFeeds(userId);
            qResult.then(function(resultant) {
                vm.feedTuple = [];
                for(var i = 0; i < resultant.data.length; i++) {
                    for(var j = 0; j < resultant.data[i].articles.length; j++) {
                        vm.feedTuple.push(resultant.data[i].articles[j]);
                    }
                }
                vm.feedTuple.sort(function(i, j) {
                    return new Date(j.publishedAt) - new Date(i.publishedAt);
                });
            });
        }

        function addTopic(name) {
            if(!name) {
                vm.error = "No topic entered.";
                return;
            }
            var newTopic = { name: name, type: "SOURCE" };
            dismissModal('#addFeedModal');
            var execution = FeedService.createFeed(userId, newTopic);
            execution.then(function(response) {
                if(!response) {
                    vm.error = "Failed to set new feed - No server response";
                } else {
                    if(response.data === "BAD NAME") {
                        vm.error = "Invalid source name: Does not exist";
                    } else {
                        vm.message = "Successfully set feed";
                        $route.reload();
                    }
                }
            }, function(error) {
                if(error.status == 400) {
                    vm.error = "Feed already exists in feed.";
                } else {
                    vm.error = "Bad server request.";
                }
            });
        }


        function renderHTML(widget) {
            return $sce.trustAsHtml(widget.text);
        }


        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/login");
                });
        }

        function fetchFeedData() {
            var interestLoad = FeedService.findAllFeedsForUser(vm.user._id);
            interestLoad.success(function(response) {
                if(response) {
                    vm.feeds = response;
                    vm.articleTuple = fetchArticleData();
                }
            });
        }

        function fetchArticleData() {
            var arr = [];
            FeedService.fetchNewsSources()
                .then(function(response) {
                    for(var i = 0; i < response.data.length; i++) {
                        arr.push(response.data[i].name);
                    }
                    vm.articleTuple = arr;
                    fetchFollowerArticles(vm.user._id);
                });
        }


        function fetchFollowerArticles(userId) {
            var query = FeedService.queryFollowers(userId);
            query.then(function(response) {
               vm.profileTuple = response.data;
               vm.profileTuple.sort(function(i, j) {
                    return new Date(j.postDate) - new Date(i.postDate);
                });
            });
        }

        function dismissModal(element) {
            $(element).on('hidden.bs.modal', function() {
                $(this).removeData('bs.modal');
            });
        }

        function removeFeed(feed) {
            var query = FeedService.deleteFeed(feed._id);
            dismissModal('#editFeedModal');
            query.then(function(response) {
                if(response) {
                    vm.message = "Successfully Deleted Feed";

                    $route.reload();
                } else {
                    vm.error = "Unable to remove feed - No server response.";
                }
            })
        }

        function setSelection(selection) {
            vm.tabSelection = selection;
        }

        function init() {
            var _user = UserService.findUserById(userId);
            _user.success(function(response) {
                vm.user = response;
                fetchFeedData();
            });
        }
        init();
        generateFeed();
    }

})();