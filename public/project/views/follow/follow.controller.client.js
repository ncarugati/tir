/**
 * Created by Nicholas on 3/28/17.
 */
(function() {
    angular
        .module("TirApp")
        .controller("FollowerController", FollowerController)
        .controller("FollowingController", FollowingController)
        .controller("FollowSearchController", FollowSearchController);

    function FollowSearchController($location, $routeParams, $rootScope, UserService) {
        var vm = this;
        vm.logout = logout;
        var searchKeyword = $routeParams["skey"];

        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/login");
                });
        }

        function init() {
            var query = UserService.searchForUser(searchKeyword);
            query.then(function(response) {
                if(response) {
                    vm.resultantTuple = response.data;
                    console.log(vm.resultantTuple);
                }
            });
        }
        init();
    }

    function FollowerController($location, $routeParams, $rootScope, UserService) {
        var vm = this;
        var userId = $routeParams["uid"];
        vm.logout = logout;

        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/login");
                });
        }

        function init() {
            var query = UserService.findUserFollowers(userId);
            query.then(function(response) {
                console.log(response.data);
                vm.followerTuple = response.data[0].followers;
            });
        }
        init();

    }

    function FollowingController($location, $routeParams, $rootScope, UserService) {
        var vm = this;
        var userId = $routeParams["uid"];

        vm.logout = logout;

        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/login");
                });
        }

        function init() {
            var query = UserService.findUserFollowing(userId);
            query.then(function(response) {
                vm.followingTuple = response.data[0].following;
            });
        }
        init();
    }

})();
