/**
 * Created by Nicholas on 4/11/17.
 */


(function() {
    angular
        .module("TirApp")
        .controller("FeedController", FeedController);


    function FeedController($routeParams, $rootScope, $route, $location, UserService, FeedService, MediaService) {
        var vm = this;
        var userId = $routeParams["uid"];

        vm.generateFeed = generateFeed;
        vm.renderHTML = renderHTML;
        vm.logout = logout;
        vm.addTopic = addTopic;
        vm.addToProfile = addToProfile;
        vm.fetchFeedData = fetchFeedData;
        vm.removeFeed = removeFeed;
        vm.shareToProfile = shareToProfile;
        vm.prepareShare = prepareShare;
        vm.toReadableDate = toReadableDate;
        vm.setSelection = setSelection;
        vm.selectedArticle = null;
        vm.tabSelection = false;


        function addToProfile(feed) {
            var nameDisplay = getNameRepresentation();
            var config = {
                "poster": nameDisplay,
                "posterId": vm.user._id,
                "author": feed.author,
                "title": feed.title,
                "description": feed.description,
                "url": feed.url,
                "image": feed.urlToImage,
                "published": feed.publishedAt,
                "postDate": new Date(Date.now())
            };

            var query = MediaService.createMedia(userId, config);
            query.then(function(resultant) {
                if(resultant) {
                    vm.message = "Successfully added to profile."
                } else {
                    vm.error = "Unable to add to profile - no server response.";
                }
                console.log(resultant);
            });

        }

        function toReadableDate(dateString) {
            return new Date(dateString).toDateString();
        }

        function prepareShare(feed) {
            vm.selectedArticle = feed;
            getFollowers();
        }

        function getNameRepresentation() {
            if(vm.user.firstName && vm.user.lastName) {
                return vm.user.firstName + " " + vm.user.lastName;
            }  else if(vm.user.firstName) {
                return vm.user.firstName;
            } else if(vm.user.lastName) {
                return vm.user.lastName;
            } else {
                return vm.user.username;
            }
        }


        function getFollowers() {
            var query = UserService.findUserFollowers(vm.user._id);
            query.then(function(response) {
                if(response) {
                    vm.followerTuple = [];
                    for(var i = 0; i < response.data[0].followers.length; i++) {
                        vm.followerTuple.push(response.data[0].followers[i].username);
                    }
                    console.log(vm.followersTuple);
                }
                getFollowing();
            });
        }

        function getFollowing() {
            var query = UserService.findUserFollowing(vm.user._id);
            query.then(function(response) {
                if(response) {
                    vm.followingTuple = [];
                    for(var i = 0; i < response.data[0].following.length; i++) {
                        vm.followingTuple.push(response.data[0].following[i].username);
                    }
                    console.log(vm.followingTuple);
                }
            });
        }

        function shareToProfile(username, feed) {
            if(!username) {
                vm.error = "No username entered.";
            }
            var nameDisplay = getNameRepresentation();

            var config = {
                "poster": nameDisplay,
                "posterId": vm.user._id,
                "author": feed.author,
                "title": feed.title,
                "description": feed.description,
                "url": feed.url,
                "image": feed.urlToImage,
                "published": feed.publishedAt,
                "postDate": new Date(Date.now())
            };

            var userQuery = UserService.findUserByUsername(username);
            userQuery.then(function(response) {
                if(!response.data) {
                    vm.error = "User does not exist";
                    return;
                }
                var query = MediaService.createMedia(response.data._id, config);
                query.then(function(resultant) {
                    if(resultant) {
                        vm.message = "Successfully added to " + response.data.username + "'s profile."
                    } else {
                        vm.error = "Unable to add to profile - no server response.";
                    }
                });
            });
        }

        function generateFeed() {
            var qResult = FeedService.queryFeeds(userId);
            qResult.then(function(resultant) {
                vm.feedTuple = [];
                for(var i = 0; i < resultant.data.length; i++) {
                    for(var j = 0; j < resultant.data[i].articles.length; j++) {
                        vm.feedTuple.push(resultant.data[i].articles[j]);
                    }
                }
                vm.feedTuple.sort(function(i, j) {
                    return new Date(i.publishedAt) - new Date(j.publishedAt);
                });
            });
        }

        function addTopic(name) {
            if(!name) {
                vm.error = "No topic entered.";
                return;
            }
            var newTopic = { name: name, type: "SOURCE" };
            var execution = FeedService.createFeed(userId, newTopic);
            execution.then(function(response) {
                if(!response) {
                    vm.error = "Failed to set new feed - No server response";
                } else {
                    if(response.data === "BAD NAME") {
                        vm.error = "Invalid source name: Does not exist";
                    } else {
                        vm.message = "Successfully set feed";
                        $route.reload();
                    }
                }
            }, function(error) {
                if(error.status == 400) {
                    vm.error = "Feed already exists in feed.";
                } else {
                    vm.error = "Bad server request.";
                }
            });
        }


        function renderHTML(widget) {
            return $sce.trustAsHtml(widget.text);
        }


        function logout() {
            UserService.logout()
                .success(function(response) {
                    $rootScope.currentUser = null;
                    $location.url("/login");
                });
        }

        function fetchFeedData() {
            var interestLoad = FeedService.findAllFeedsForUser(vm.user._id);
            interestLoad.success(function(response) {
                if(response) {
                    vm.feeds = response;
                    vm.articleTuple = fetchArticleData();
                }
            });
        }

        function fetchArticleData() {
            var arr = [];
            FeedService.fetchNewsSources()
                .then(function(response) {
                    for(var i = 0; i < response.data.length; i++) {
                        arr.push(response.data[i].name);
                    }
                    vm.articleTuple = arr;
                    fetchFollowerArticles(vm.user._id);
                });
        }


        function fetchFollowerArticles(userId) {
            var query = FeedService.queryFollowers(userId);
            query.then(function(response) {
                vm.profileTuple = response.data;
                vm.profileTuple.sort(function(i, j) {
                    return new Date(i.postDate) - new Date(j.postDate);
                });
            });
        }

        function removeFeed(feed) {
            var query = FeedService.deleteFeed(feed._id);
            query.then(function(response) {
                if(response) {
                    vm.message = "Successfully Deleted Feed";
                    $route.reload();
                } else {
                    vm.error = "Unable to remove feed - No server response.";
                }
            })
        }

        function setSelection(selection) {
            vm.tabSelection = selection;
        }

        function init() {
            var _user = UserService.findUserById(userId);
            _user.success(function(response) {
                vm.user = response;
                fetchFeedData();
            });
        }
        init();
        generateFeed();
    }

})();