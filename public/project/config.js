/**
 * Created by Nicholas on 3/28/17.
 */
(function () {
    angular
        .module("TirApp")
        .config(Config);

    var checkLoggedin = function($q, $timeout, $http, $location, $rootScope) {
        var deferred = $q.defer();
        $http.get('/api/loggedin').success(function(user) {
            $rootScope.errorMessage = null;
            if (user !== '0') {
                $rootScope.currentUser = user;
                deferred.resolve();
            } else {
                deferred.reject();
                $location.url('/login');
            }
        });
        return deferred.promise;
    };

    var checkSession = function($q, $timeout, $http, $location, $rootScope) {
        var deferred = $q.defer();
        $http.get('/api/loggedin').success(function(user) {
            $rootScope.errorMessage = null;
            if (user !== '0') {
                $rootScope.currentUser = user;
                deferred.resolve();
            } else {
                deferred.reject();
            }
        });
    };

    var checkAdmin = function($q, $timeout, $http, $location, $rootScope) {
        var deferred = $q.defer();
        $http.get('/api/loggedin').success(function(user) {
            $rootScope.errorMessage = null;
            if (user !== '0') {
                $rootScope.currentUser = user;
                if($rootScope.currentUser.rights) {
                    deferred.resolve();
                } else {
                    deferred.reject();
                    $location.url('/login');
                }
            } else {
                deferred.reject();
                $location.url('/login');
            }
        });
        return deferred.promise;
    };

    function Config($routeProvider, $stateProvider) {
        $routeProvider
            .when("/", {
                templateUrl: "views/user/launch.html",
                controller: "LaunchController",
                controllerAs: "model"
            })
            .when("/login", {
                templateUrl: "views/user/login.html",
                controller: "LoginController",
                controllerAs: "model"
             })
            .when("/register", {
                templateUrl: "views/user/register.html",
                controller: "RegisterController",
                controllerAs: "model"
            })
            .when("/user/:uid", {
                templateUrl: "views/feed/feed.html",
                controller: "FeedController",
                controllerAs: "model",
                resolve: { loggedin: checkLoggedin }
            })
            .when("/user/edit/:uid", {
                templateUrl: "views/user/profile-edit.html",
                controller: "ProfileEditController",
                controllerAs: "model",
                resolve: { loggedin: checkLoggedin }
            })
            .when("/user/profile/:uid", {
                templateUrl: "views/user/profile.html",
                controller: "ProfileController",
                controllerAs: "model",
                resolve: { loggedin: checkSession }
            })
            .when("/user/profile/:uid/followers", {
                templateUrl: "views/follow/followers.html",
                controller: "FollowerController",
                controllerAs: "model",
                resolve: { loggedin: checkLoggedin }
            })
            .when("/user/profile/:uid/following", {
                templateUrl: "views/follow/following.html",
                controller: "FollowingController",
                controllerAs: "model",
                resolve: { loggedin: checkLoggedin }
            })
            .when("/search/:skey", {
                templateUrl: "views/follow/follower-search.html",
                controller: "FollowSearchController",
                controllerAs: "model",
                resolve: { loggedin: checkSession }
            })
            .when("/user/:uid/admin", {
                templateUrl: "views/admin/admin-main.html",
                controller: "AdminController",
                controllerAs: "model",
                resolve: { loggedin: checkAdmin }
            });
    }
})();
