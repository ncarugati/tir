/**
 * Created by Nicholas on 4/11/17.
 */
(function() {
    angular
        .module("TirApp")
        .factory("RatingService", RatingService);

    function RatingService($http) {

        function createRating(mediaId, rate) {
            return $http.post("/api/rating/" + mediaId, rate);
        }

        function findRatingByUser(userId, mediaId) {
            return $http.get("/api/rating/" + mediaId + "/user/" + userId);
        }

        function computeMediaRating(mediaId) {
            return $http.get("/api/rating/" + mediaId);
        }

        function updateRating(userId, mediaId, value) {
            return $http.put("/api/rating/" + mediaId + "/user/" + userId, value);
        }

        function deleteRating(userId, mediaId) {
            return $http.delete("/api/rating/" + mediaId + "/user/" + userId);
        }

        var api = {
            "createRating"           : createRating,
            "findRatingByUser"       : findRatingByUser,
            "computeMediaRating"     : computeMediaRating,
            "updateRating"           : updateRating,
            "deleteRating"           : deleteRating
        };
        return api;
    }

})();