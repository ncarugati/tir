/**
 * Created by Nicholas on 4/8/17.
 */
(function() {
    angular
        .module("TirApp")
        .factory("AdminService", AdminService);

    function AdminService($http) {

        function getAllUsers() {
            return $http.get("/api/admin");
        }

        function getAllArticles() {
            return $http.get("/api/admin/articles");
        }

        function setRights(user, bool) {
            return $http.put("/api/admin?rights=" + bool, user);
        }

        function getAllComments() {
            return $http.get("/api/admin/comments");
        }

        function createUser(user) {
            return $http.post("/api/admin/user", user);
        }

        var api = {
            "getAllUsers"   : getAllUsers,
            "getAllArticles": getAllArticles,
            "setRights"     : setRights,
            "getAllComments": getAllComments,
            "createUser"    : createUser
        };
        return api;
    }

})();