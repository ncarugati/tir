/**
 * Created by Nicholas on 4/9/17.
 */
(function() {
    angular
        .module("TirApp")
        .factory("CommentService", CommentService);

    function CommentService($http) {

        function createComment(mediaId, comment) {
            return $http.post("/api/comment/" + mediaId, comment);
        }

        function findAllCommentsForUser(userId) {
            return $http.get("/api/comment/user/" + userId);
        }

        function findAllCommentsForMedia(mediaId) {
            return $http.get("/api/comment/" + mediaId);
        }

        function updateComment(commentId, comment) {
            return $http.put("/api/comment/" + commentId, comment);
        }

        function deleteComment(commentId) {
            return $http.delete("/api/comment/" + commentId);
        }

        var api = {
            "createComment"           : createComment,
            "findAllCommentsForUser"  : findAllCommentsForUser,
            "findAllCommentsForMedia" : findAllCommentsForMedia,
            "updateComment"           : updateComment,
            "deleteComment"           : deleteComment
        };
        return api;
    }

})();