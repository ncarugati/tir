/**
 * Created by Nicholas on 3/28/17.
 */
(function() {
    angular
        .module("TirApp")
        .factory("UserService", UserService);

    function UserService($http) {

        var api = {
            "createUser"                : createUser,
            "findUserById"              : findUserById,
            "findUserByUsername"        : findUserByUsername,
            "findUserByCredentials"     : findUserByCredentials,
            "updateUser"                : updateUser,
            "deleteUser"                : deleteUser,
            "logout"                    : logout,
            "login"                     : login,
            "register"                  : register,
            "loggedIn"                  : loggedIn,
            "follow"                    : follow,
            "unfollow"                  : unfollow,
            "gainFollower"              : gainFollower,
            "loseFollower"              : loseFollower,
            "findUserFollowers"         : findUserFollowers,
            "findUserFollowing"         : findUserFollowing,
            "searchForUser"             : searchForUser
        };
        return api;

        function createUser(user) {
            return $http.post("/api/user", user);
        }

        function findUserById(userId) {
            return $http.get("/api/user/" + userId);
        }

        function findUserByUsername(username) {
            return $http.get("/api/user?username=" + username);
        }

        function findUserByCredentials(username, password) {
            return $http.get("/api/user?username=" + username + "&password=" + password);
        }

        function updateUser(userId, user, newPass) {
            if(newPass) {
                return $http.put("/api/user/" + userId + "?password=" + newPass, user);
            }
            return $http.put("/api/user/" + userId, user);
        }

        function deleteUser(userId) {
            return $http.delete("/api/user/" + userId);
        }

        function login(user) {
            return $http.post("/api/login", user);
        }

        function loggedIn() {
            return $http.get("/api/loggedIn");
        }

        function logout() {
            return $http.post("/api/logout");
        }

        function register(user) {
            return $http.post("/api/register", user);
        }

        function follow(userId, user) {
            return $http.put("/api/user/" + userId + "/flw", user);
        }

        function unfollow(userId, user) {
            return $http.put("/api/user/" + userId + "/unf", user);
        }

        function gainFollower(userId, user) {
            return $http.put("/api/user/" + userId + "/gf", user);
        }

        function loseFollower(userId, user) {
            return $http.put("/api/user/" + userId + "/lf", user);
        }

        function findUserFollowers(userId) {
            return $http.get("/api/user/" + userId + "/followers");
        }

        function findUserFollowing(userId) {
            return $http.get("/api/user/" + userId + "/following");
        }

        function searchForUser(keyword) {
            return $http.get("/api/search/" + keyword);
        }


    }
})();
