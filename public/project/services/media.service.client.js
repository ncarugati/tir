/**
 * Created by Nicholas on 3/28/17.
 */
(function() {
    angular
        .module("TirApp")
        .factory("MediaService", MediaService);

    function MediaService($http) {

        var api = {
            "createMedia"               : createMedia,
            "findAllMediaForProfile"    : findAllMediaForProfile,
            "findMediaById"             : findMediaById,
            "updateMedia"               : updateMedia,
            "deleteMedia"               : deleteMedia
        };
        return api;

        function createMedia(userId, media) {
            media.userId = userId;
            return $http.post("/api/media/" + userId + "/media", media);
        }

        function findAllMediaForProfile(userId) {
            return $http.get("/api/media/" + userId + "/media");
        }

        function findMediaById(mediaId) {
            return $http.get("/api/media/" + mediaId);
        }

        function updateMedia(mediaId, media) {
            return $http.put("/api/media/" + mediaId, media);
        }

        function deleteMedia(mediaId) {
            return $http.delete("/api/media/" + mediaId);
        }

    }
})();