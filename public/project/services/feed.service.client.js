/**
 * Created by Nicholas on 4/5/17.
 */
(function() {
    angular
        .module("TirApp")
        .factory("FeedService", FeedService);

    function FeedService($http) {

        var api = {
            "createFeed"                : createFeed,
            "findAllFeedsForUser"       : findAllFeedsForUser,
            "findFeedById"              : findFeedById,
            "queryFeeds"                : queryFeeds,
            "queryFollowers"            : queryFollowers,
            "deleteFeed"                : deleteFeed,
            "fetchNewsSources"          : fetchNewsSources
        };
        return api;

        function createFeed(userId, feed) {
            feed._user = userId;
            return $http.post("/api/feed/" + userId, feed);
        }

        function queryFeeds(userId) {
            return $http.get("/api/feed/" + userId + "/all");
        }

        function queryFollowers(userId) {
            return $http.get("/api/feed/" + userId + "/follow");
        }

        function findAllFeedsForUser(userId) {
            return $http.get("/api/feed/" + userId + "/feed");
        }

        function findFeedById(feedId) {
            return $http.get("/api/feed/" + feedId);
        }

        function deleteFeed(feedId) {
            return $http.delete("/api/feed/" + feedId);
        }

        function fetchNewsSources() {
            return $http.get('data/news-sources.json');
        }

    }
})();