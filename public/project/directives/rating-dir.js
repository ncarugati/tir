/**
 * Created by Nicholas on 4/11/17.
 */
(function () {
    angular
        .module('TirApp')
        .directive('ratingWidget', ['$routeParams', ratingWidget]);

    function ratingWidget() {

        function linkFunc(scope, element, attributes) {
            if(attributes.media) {
                scope.model.initialize(attributes.media);
            }
        }

        return {
            scope: {},
            link: linkFunc,
            controller: RatingController,
            controllerAs: "model",
            templateUrl: "views/rating/rating.html"
        };
    }

    function RatingController($rootScope, RatingService) {
        var vm = this;
        vm.rate = rate;
        vm.initialize = initialize;

        function initialize(mediaId) {
            var query = RatingService.findRatingByUser($rootScope.currentUser._id, mediaId);
            query.then(function(response) {
                vm.rating = !response.data ? null : response.data;
                vm.mediaId = mediaId;
                computeMediaRating(vm.mediaId);
            });
        }

        function rate(value) {
            if(vm.rating != null) {
                if(vm.rating.value == value) {
                    removeRating(vm.mediaId);
                } else {
                    updateRating(vm.mediaId, value);
                }
            } else {
                createRating(vm.mediaId, value);
            }
        }

        function createRating(mediaId, value) {
            var config = {
                "userId": $rootScope.currentUser._id,
                "value": value
            };
            var query = RatingService.createRating(mediaId, config);
            query.then(function(response) {
               if(response) {
                   refreshRating();
               }
            });
        }

        function removeRating(mediaId) {
            var query = RatingService.deleteRating($rootScope.currentUser._id, mediaId);
            query.then(function(response) {
                if(response) {
                    refreshRating();
                }
            });
        }

        function updateRating(mediaId, value) {
            var query = RatingService.updateRating($rootScope.currentUser._id, mediaId, {"value": value});
            query.then(function(response) {
                if(response) {
                    refreshRating();
                }
            });
        }

        function refreshRating() {
            var query = RatingService.findRatingByUser($rootScope.currentUser._id, vm.mediaId);
            query.then(function(response) {
                vm.rating = !response.data ? null : response.data;
                computeMediaRating(vm.mediaId);
            });
        }

        function computeMediaRating(mediaId) {
            var query = RatingService.computeMediaRating(mediaId);
            query.then(function(response) {
                if(response) {
                    vm.ratingCount = response.data;
                }
            });
        }
    }
})();