/**
 * Created by Nicholas on 4/9/17.
 */
(function () {
    angular
        .module('TirApp')
        .directive('commentMedia', ['$routeParams', commentMedia]);

    function commentMedia() {

        function linkFunc(scope, element, attributes) {
            if(attributes.data) {
                scope.model.getMediaCommentTuple(attributes.data);
            }
        }

        return {
            scope: {},
            link: linkFunc,
            controller: CommentController,
            controllerAs: "model",
            templateUrl: "views/comment/comments.html"
        };
    }

    //NOTE: Scope had to be used here because there had to be some form of a
    // bridged interaction for data between two controllers
    function CommentController($scope, $route, $rootScope, CommentService) {
        var vm = this;

        vm.getMediaCommentTuple = getMediaCommentTuple;
        vm.removeComment = removeComment;
        vm.editComment = editComment;
        vm.isCommentOwner = isCommentOwner;
        vm.toReadableDate = toReadableDate;

        function getMediaCommentTuple(id) {
            CommentService.findAllCommentsForMedia(id)
                .then(function(response) {
                    $scope.commentsTuple = response.data;
                });
        }

        function editComment(commentId, comment) {
            if(!comment.text) {
                $scope.isEditing = false;
                return;
            }
            var query = CommentService.updateComment(commentId, comment);
            query.then(function(response) {
                if(response) {
                    $scope.isEditing = false;
                }
            });
        }

        function isCommentOwner(comment) {
            if(!$rootScope.currentUser) {
                return false;
            }
            if($rootScope.currentUser.rights) {
                return true;
            }
            return comment.userId === $rootScope.currentUser._id;
        }

        function removeComment(commentId) {
            var query = CommentService.deleteComment(commentId);
            query.then(function(response) {
                if(response) {
                    vm.message = "Comment successfully deleted";
                    $route.reload();
                } else {
                    vm.error = "Failed to delete comment";
                }
            })
        }

        function toReadableDate(dateString) {
            return new Date(dateString).toDateString();
        }

    }



})();