/**
 * Created by Nicholas on 3/10/17.
 */


module.exports = function() {

    var mongoose = require("mongoose");

    var WidgetSchema = mongoose.Schema({
        _page       : {type: mongoose.Schema.ObjectId, ref: 'pageModel'},
        type        : {type: String, enum:['HEADER', 'IMAGE', 'YOUTUBE', 'HTML', 'INPUT', 'TEXT', 'NULL']},
        name        : String,
        text        : String,
        placeholder : String,
        description : String,
        url         : String,
        width       : String,
        height      : String,
        rows        : {type: Number, default: 1},
        size        : {type: Number, default: 1},
        class       : String,
        icon        : String,
        deletable   : Boolean,
        orderId     : {type: Number, default: 0},
        formatted   : Boolean
    }, {collection  : 'widgetmodels'});
    return WidgetSchema;
};