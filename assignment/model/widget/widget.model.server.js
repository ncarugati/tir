/**
 * Created by Nicholas on 3/10/17.
 */


module.exports = function() {

    var mongoose = require("mongoose");
    var WidgetSchema = require("./widget.schema.server")(mongoose);
    var WidgetModel = mongoose.model("widgetModel", WidgetSchema);

    function createWidget(pageId, widget) {
        var query = {
            "type"          : widget.type,
            "orderId"       : widget.orderId,
            "_page"         : pageId
        };
        return WidgetModel.create(query);
    }

    function findAllWidgetsForPage(pageId) {
        return WidgetModel.find({_page: pageId}).populate('_page').sort('orderId').exec();
    }

    function findWidgetById(widgetId) {
        return WidgetModel.findOne({_id: widgetId});
    }

    function updateOrderOnAdd(pageId) {
        return WidgetModel.find({_page: pageId}).populate('_page').exec(function(error, result) {
            for(var widget = 0, len = result.length; widget < len; widget++) {
                result[widget].orderId++;
                result[widget].save();
            }
        });
    }

    function updateOrderOnRemoval(pageId) {
        return WidgetModel.find({_page: pageId}).populate('_page').exec(function(error, result) {
            for(var widget = 0, len = result.length; widget < len; widget++) {
                if(result[widget].orderId > widget + 1) {
                    result[widget].orderId--;
                    result[widget].save();
                }
            }
        });
    }

    function updateWidget(widgetId, widget) {
        return WidgetModel.update({_id: widgetId}, {$set: widget});
    }

    function deleteWidget(widgetId) {
        return WidgetModel.remove({_id: widgetId});
    }

    function reorderWidget(pageId, start, end) {
        return WidgetModel.find({_page: pageId}).populate('_page').exec(function(error, result) {
            for(var widget = 0, len = result.length; widget < len; widget++) {
                if (result[widget].orderId == start) {
                    result[widget].orderId = end;
                    result[widget].save();
                } else if (result[widget].orderId == end) {
                    result[widget].orderId = start;
                    result[widget].save();
                }
            }
        });
    }

    var api = {
        createWidget           : createWidget,
        findAllWidgetsForPage  : findAllWidgetsForPage,
        findWidgetById         : findWidgetById,
        updateWidget           : updateWidget,
        deleteWidget           : deleteWidget,
        reorderWidget          : reorderWidget,
        updateOrderOnAdd       : updateOrderOnAdd,
        updateOrderOnRemoval   : updateOrderOnRemoval
    };
    return api;
};