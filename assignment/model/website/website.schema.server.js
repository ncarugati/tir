/**
 * Created by Nicholas on 3/10/17.
 */

module.exports = function() {

    var mongoose = require("mongoose");

    var PageSchema = require("../page/page.schema.server.js")();

    var WebsiteSchema = mongoose.Schema({
        _user: {type: mongoose.Schema.ObjectId, ref: 'userModel'},
        name: String,
        description: String,
        pages: [PageSchema]
    }, { collection: 'websitemodels'} );

    return WebsiteSchema;
};