/**
 * Created by Nicholas on 3/10/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");
    var WebsiteSchema = require("./website.schema.server")(mongoose);
    var WebsiteModel = mongoose.model("modelWebsite", WebsiteSchema);

    function createWebsiteForUser(userId, website) {
        var query = {
            "name": website.name,
            "_user": userId,
            "description": website.description
        };
        return WebsiteModel.create(query);
    }

    function findAllWebsitesForUser(userId) {
        return WebsiteModel.find({_user: userId});
    }

    function findWebsiteById(websiteId) {
        return WebsiteModel.findOne({_id: websiteId});
    }

    function updateWebsite(websiteId, website) {
        return WebsiteModel.update({_id: websiteId}, {$set: website});
    }

    function deleteWebsite(websiteId) {
        return WebsiteModel.remove({_id: websiteId});
    }

    var api = {
        createWebsiteForUser    : createWebsiteForUser,
        findAllWebsitesForUser  : findAllWebsitesForUser,
        findWebsiteById         : findWebsiteById,
        updateWebsite           : updateWebsite,
        deleteWebsite           : deleteWebsite
    };
    return api;
};
