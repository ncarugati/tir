/**
 * Created by Nicholas on 3/10/17.
 */

module.exports = function() {

    var mongoose = require("mongoose");
    var WebsiteSchema = require("../website/website.schema.server.js")();

    var UserSchema = mongoose.Schema({
        username: String,
        password: String,
        firstName: String,
        lastName: String,
        email: String,
        websites: [WebsiteSchema],
        facebook: {
            id:    String,
            token: String
        }
    }, { collection: 'usermodels' });

    return UserSchema;
};