/**
 * Created by Nicholas on 3/10/17.
 */

module.exports = function() {
    var mongoose = require("mongoose");

    var WidgetSchema = require("../widget/widget.schema.server.js")();

    var PageSchema = mongoose.Schema({
        _website: {type: mongoose.Schema.ObjectId, ref: 'websiteModel'},
        name: String,
        title: String,
        description: String,
        widgets: [WidgetSchema]
    }, {collection: 'pagemodels'});

    return PageSchema;
};


