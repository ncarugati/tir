/**
 * Created by Nicholas on 3/10/17.
 */

module.exports = function() {

    var mongoose = require("mongoose");
    var PageSchema = require("./page.schema.server")(mongoose);
    var PageModel = mongoose.model("pageModel", PageSchema);

    function createPage(websiteId, page) {
        var query = {
            "_website": websiteId,
            "name": page.name,
            "title": page.title,
            "description": page.description
        };
        return PageModel.create(query);
    }

    function findAllPagesForWebsite(websiteId) {
        return PageModel.find({_website: websiteId});
    }

    function findPageById(pageId) {
        return PageModel.findOne({_id: pageId});
    }

    function updatePage(pageId, page) {
        return PageModel.update({_id: pageId}, {$set: page});
    }

    function deletePage(pageId) {
        return PageModel.remove({_id: pageId});
    }

    var api = {
        createPage              : createPage,
        findAllPagesForWebsite  : findAllPagesForWebsite,
        findPageById            : findPageById,
        updatePage              : updatePage,
        deletePage              : deletePage
    };
    return api;
};
