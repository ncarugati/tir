/**
 * Created by Nicholas on 1/30/17.
 */
module.exports = function(app, models) {

    var modelPage = models.pageModel;
    app.post("/api/website/:websiteId/page", createPage);
    app.get("/api/website/:websiteId/page", findPageByWebsiteId);
    app.get("/api/page/:pageId", findPageById);
    app.put("/api/page/:pageId", updatePage);
    app.delete("/api/page/:pageId", deletePage);

    function createPage(req, res) {
        var websiteId = req.params.websiteId;
        var page = req.body;
        page.websiteId = websiteId;
        modelPage.createPage(websiteId, page)
            .then(
                function(pageCreation) {
                   res.json(pageCreation);
                }
            );
    }

    function findPageByWebsiteId(req, res) {
        var websiteId = req.params.websiteId;
        modelPage.findAllPagesForWebsite(websiteId)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function findPageById(req, res) {
        var pageId = req.params.pageId;
        modelPage.findPageById(pageId)
            .then(
                function(result) {
                    res.json(result);
                }
            );
    }

    function updatePage(req, res) {
        var pageId = req.params.pageId;
        var page = req.body;
        modelPage.updatePage(pageId, page)
            .then(
                function(updatePage) {
                    res.json(updatePage);
                }
            );
    }

    function deletePage(req, res) {
        var pageId = req.params.pageId;
        modelPage.deletePage(pageId)
            .then(
                function(pageDeletion) {
                    res.json(pageDeletion);
                }
            );
    }
};