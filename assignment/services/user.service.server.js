/**
 * Created by Nicholas on 1/30/17.
 */

var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var bcrypt = require("bcrypt-nodejs");

module.exports = function(app, models) {

    var modelUser = models.userModel;

    var facebookConfigHeroku = {
        clientID     : "1924590687774075",
        clientSecret : "6ddc4310db9de6b1f587c2acd4125c0e",
        callbackURL  : "https://nicholas-carugati-assignment.herokuapp.com/auth/facebook/callback"
    };

    passport.serializeUser(serializeUser);
    passport.deserializeUser(deserializeUser);
    passport.use('wam', new LocalStrategy(localStrategy));
    passport.use(new FacebookStrategy(facebookConfigHeroku, facebookStrategy));


    app.post("/api/user", createUser);
    app.get("/api/user", findUser);
    app.get("/api/user/:userId", findUserById);
    app.put("/api/user/:userId", updateUser);
    app.delete("/api/user/:userId", deleteUser);
    app.post('/api/login', passport.authenticate('wam'), login);
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));
    app.post('/api/logout', logout);
    app.post('/api/register', register);
    app.get('/api/loggedin', loggedIn);

    //Custom callback provided by: http://passportjs.org/docs
    app.get('/auth/facebook/callback', function(req, res, next) {
        passport.authenticate('facebook', function(err, user, info) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.redirect('/assignment/#/login');
            }
            req.logIn(user, function(err) {
                return res.redirect('/assignment/#/user/' + user._id);
            });
        })(req, res, next);
    });

    function loggedIn(req, res) {
        res.send(req.isAuthenticated() ? req.user : '0');
    }

    function createUser(req, res) {
        var user = req.body;
        user.password = bcrypt.hashSync(user.password);
        modelUser.createUser(user)
            .then(
                function(newUser) {
                    res.json(newUser);
                },
                function(error) {
                    console.log(error);
                }
            );
    }

    function findUserById(req, res) {
        var id = req.params.userId;
        modelUser.findUserById(id)
            .then(
                function(userFound) {
                   res.json(userFound);
                }
            );
    }

    function findUser(req, res) {
        var query = req.query;
        if(query.password && query.username) {
            findUserByCredentials(query.username, query.password, res);
        } else if(query.username) {
            findUserByUsername(query.username, res);
        } else {
            res.sendStatus(400);
        }
    }



    function findUserByUsername(username, res) {
        modelUser.findUserByUsername(username)
            .then(
                function(userFound) {
                    res.json(userFound);
                }
            );
    }

    function findUserByCredentials(username, password, res) {
        modelUser.findUserByCredentials(username, password)
            .then(
                function(userFound) {
                   res.json(userFound);
                }
            );
    }

    function updateUser(req, res) {
        var id = req.params.userId;
        var user = req.body;
        modelUser.updateUser(id, user)
            .then(
                function(userUpdate) {
                    res.json(userUpdate);
                }
            );
    }

    function deleteUser(req, res) {
        var id = req.params.userId;
        modelUser.deleteUser(id)
            .then(
                function(userDeletion) {
                    res.json(userDeletion);
                }
            );
    }

    function localStrategy(username, password, done) {
        modelUser.findUserByUsername(username)
            .then(
                function(user) {
                    if(user && bcrypt.compareSync(password, user.password)) {
                        return done(null, user);
                    } else {
                        return done(null, false);
                    }
                },
                function(err) {
                    if (err) { return done(err); }
                }
            );
    }

    function facebookStrategy(token, refreshToken, profile, done) {
        modelUser.findUserByFacebookId(profile.id)
            .then(
                function (facebookCredentials) {
                    if(facebookCredentials) {
                        return done(null, facebookCredentials);
                    } else {
                        facebookCredentials = {
                            username: profile.displayName.replace(/ /g, ''),
                            facebook: {
                                id:    profile.id,
                                token: token
                            }
                        };
                        modelUser.createUser(facebookCredentials)
                            .then(
                                function (user) {
                                    return done(null, user);
                                }
                            );
                    }
                }
            )
            .then(
                function(user) {
                    return done(null, user);
                }
            );
    }


    function serializeUser(user, done) {
        done(null, user);
    }


    function deserializeUser(user, done) {
        modelUser.findUserById(user._id)
            .then(
                function(user){
                    done(null, user);
                },
                function(err){
                    done(err, null);
                }
            );
    }

    function login(req, res) {
        var user = req.user;
        res.json(user);
    }

    function logout(req, res) {
        req.logOut();
        res.send(200);
    }


    function register(req, res) {
        var user = req.body;
        modelUser.createUser(user)
            .then(function(user){
                    if(user){
                        req.login(user, function(err) {
                            if(err) {
                                res.status(400).send(err);
                            } else {
                                res.json(user);
                            }
                        });
                    }
                }
            );
    }

};