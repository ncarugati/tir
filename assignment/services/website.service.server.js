/**
 * Created by Nicholas on 1/30/17.
 */

module.exports = function(app, models) {

    var modelWebsite = models.websiteModel;
    app.post("/api/user/:userId/website", createWebsite);
    app.get("/api/user/:userId/website", findWebsitesByUser);
    app.get("/api/website/:websiteId", findWebsiteById);
    app.put("/api/website/:websiteId", updateWebsite);
    app.delete("/api/website/:websiteId", deleteWebsite);

    function createWebsite(req, res) {
        var userId = req.params.userId;
        var website = req.body;
        website.developerId = userId;
        modelWebsite.createWebsiteForUser(userId, website)
            .then(
                function(webCreate) {
                    res.json(webCreate);
                }
            );
    }

    function findWebsitesByUser(req, res) {
        var userId = req.params.userId;
        modelWebsite.findAllWebsitesForUser(userId)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function findWebsiteById(req, res) {
        var websiteId = req.params.websiteId;
        modelWebsite.findWebsiteById(websiteId)
            .then(
                function(website) {
                    res.json(website);
                }
            );
    }

    function updateWebsite(req, res) {
        var websiteId = req.params.websiteId;
        var website = req.body;
        modelWebsite.updateWebsite(websiteId, website)
            .then(
                function(updatedWebsite) {
                    res.json(updatedWebsite);
                }
            );
    }

    function deleteWebsite(req, res) {
        var websiteId = req.params.websiteId;
        modelWebsite.deleteWebsite(websiteId)
            .then(
                function(deletedWebsite) {
                    res.json(deletedWebsite);
                }
            );
    }
};