/**
 * Created by Nicholas on 1/30/17.
 */

module.exports = function(app, models) {

    var modelWidget = models.widgetModel;
    var multer = require('multer'); // npm install multer --save
    var upload = multer({ dest: __dirname+'/../../public/uploads' });
    var deletionFlag = false;

    app.post("/api/page/:pageId/widget", createWidget);
    app.get("/api/page/:pageId/widget", findWidgetsByPageId);
    app.get("/api/widget/:widgetId", findWidgetById);
    app.put("/api/widget/:widgetId", updateWidget);
    app.put("/api/page/:pageId/widget", sortWidget);
    app.delete("/api/widget/:widgetId", deleteWidget);
    app.post("/api/upload", upload.single('myFile'), uploadImage);

    function createWidget(req, res) {
        var pageId = req.params.pageId;
        var widget = req.body;
        widget.pageId = pageId;

        modelWidget.createWidget(pageId, widget)
            .then(
                function(widgetCreation) {
                    res.json(widgetCreation);
                }
            );
        modelWidget.updateOrderOnAdd(pageId).then(function(result){});
    }

    function findWidgetsByPageId(req, res) {
        var pageId = req.params.pageId;
        if(deletionFlag) {
            modelWidget.updateOrderOnRemoval(pageId).then(function(result){});
            deletionFlag = false;
        }
        modelWidget.findAllWidgetsForPage(pageId)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function findWidgetById(req, res) {
        var widgetId = req.params.widgetId;
        modelWidget.findWidgetById(widgetId)
            .then(
                function(widgetQuery) {
                    res.json(widgetQuery);
                }
            );
    }

    function updateWidget(req, res) {
        var widgetId = req.params.widgetId;
        var widget = req.body;
        modelWidget.updateWidget(widgetId, widget)
            .then(
                function(updatedWidget) {
                    res.json(updatedWidget);
                }
            );
    }

    function deleteWidget(req, res) {
        var widgetId = req.params.widgetId;
        modelWidget.deleteWidget(widgetId)
            .then(
                function(widgetDeletion) {
                    res.json(widgetDeletion);
                }
            );
        deletionFlag = true;
    }

    function sortWidget(req, res) {
        var pageId = req.params.pageId;
        var query = req.query;
        if(query.initial && query.final) {
            var init = parseInt(query.initial);
            var fin = parseInt(query.final);
            modelWidget.reorderWidget(pageId, init, fin);
            res.sendStatus(200);
        } else {
            res.sendStatus(400);
        }
    }



    function uploadImage(req, res) {

        var widgetId      = req.body.widgetId;
        var width         = req.body.width;
        var myFile        = req.file;

        var urlPayload = "/assignment/#/user/"+ req.body.userId + "/website/" + req.body.websiteId
            + "/page/" + req.body.pageId + "/widget/" + widgetId;

        if(!myFile) {
            res.redirect(urlPayload);
            return;
        }

        var originalname  = myFile.originalname; // file name on user's computer
        var filename      = myFile.filename;     // new file name in upload folder
        var path          = myFile.path;         // full path of uploaded file
        var destination   = myFile.destination;  // folder where file is saved to
        var size          = myFile.size;
        var mimetype      = myFile.mimetype;

        modelWidget.updateWidget(widgetId, {url: "/uploads/" + filename})
            .then(
                function(widget) {
                    if(widget) {
                        res.redirect(urlPayload);
                    }
                }
            );




    }


};