/**
 * Created by Nicholas on 4/11/17.
 */
module.exports = function(app, models) {

    var ratingModel = models.ratingModel;

    app.post("/api/rating/:mediaId", createRating);
    app.get("/api/rating/:mediaId/user/:userId", findRatingByUser);
    app.get("/api/rating/:mediaId", computeMediaRating);
    app.put("/api/rating/:mediaId/user/:userId", updateRating);
    app.delete("/api/rating/:mediaId/user/:userId", deleteRating);


    function createRating(req, res) {
        var mediaId = req.params.mediaId;
        var rate = req.body;
        ratingModel.createRating(mediaId, rate)
            .then(
                function(feedCreation) {
                    res.json(feedCreation);
                }
            );
    }


    function findRatingByUser(req, res) {
        var userId = req.params.userId;
        var mediaId = req.params.mediaId;
        ratingModel.findRatingByUser(userId, mediaId)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }


    function computeMediaRating(req, res) {
        var mediaId = req.params.mediaId;
        ratingModel.findAllRatingsForMedia(mediaId)
            .then(
                function(resultant) {
                    var finalRating = 0;
                    for(var i = 0; i < resultant.length; i++) {
                        if(resultant[i].value) {
                            finalRating++;
                        } else {
                            finalRating--;
                        }
                    }
                    res.send("" + finalRating);
                }
            );
    }

    function updateRating(req, res) {
        var userId = req.params.userId;
        var mediaId = req.params.mediaId;
        var value = req.body.value;
        ratingModel.updateRating(userId, mediaId, value)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function deleteRating(req, res) {
        var userId = req.params.userId;
        var mediaId = req.params.mediaId;
        ratingModel.deleteRating(userId, mediaId)
            .then(
                function(mediaDeletion) {
                    res.json(mediaDeletion);
                }
            );
    }




};