/**
 * Created by Nicholas on 4/5/17.
 */
module.exports = function(app, models) {

    var feedModel = models.feedModel;
    var userModel = models.userModel;
    var profileModel = models.profileModel;

    var NewsAPI = require('newsapi');
    var csv = require('csv-to-array');
    var prom = require('bluebird');
    var api = new NewsAPI('848f98b3aadd4c6bb414fd771feb69a7');

    app.post("/api/feed/:userId", createFeed);
    app.get("/api/feed/:userId/feed", findAllFeedsForUser);
    app.get("/api/feed/:feedId", findFeedById);
    app.get("/api/feed/:userId/all", queryFeeds);
    app.get("/api/feed/:userId/follow", queryFollowers);
    app.delete("/api/feed/:feedId", deleteFeed);

    function createFeed(req, res) {
        var userId = req.params.userId;
        var feed = req.body;
        feed.userId = userId;
        feedModel.findAllFeedsForUser(userId)
            .then(function(result) {
                var dupCheck = result;
                for(var i = 0; i < dupCheck.length; i++) {
                    if(dupCheck[i].name.toLowerCase() === req.body.name.toLowerCase()) {
                        res.sendStatus(400);
                        return;
                    }
                }
                var columns = ["name", "value"];
                csv({
                    file: "project/data/news-sources.csv",
                    columns: columns
                }, function (err, array) {
                    var dataset = array;
                    for(var i = 0; i < dataset.length; i++) {
                        if(dataset[i].name.toLowerCase() === req.body.name.toLowerCase()) {
                            req.body.name = dataset[i].name;
                            req.body.value = dataset[i].value;
                            break;
                        }
                    }
                    if(!req.body.value) {
                        res.send("BAD NAME");
                    } else {
                        feedModel.createFeed(userId, feed)
                            .then(
                                function(feedCreation) {
                                    res.json(feedCreation);
                                }
                            );
                    }
                });

            });
    }


    function queryFollowers(req, res) {
        var userId = req.params.userId;
        var followQuery = userModel.findUserFollowing(userId);
        var finalTuple = [];
        followQuery.then(function(resultant) {
           var promises = [];
           var followTuple = resultant[0].following;
           for(var i = 0; i < followTuple.length; i++) {
                var flwId = followTuple[i].uid;
                var feedQuery = profileModel.findAllMediaForProfile(flwId)
                    .then(function(tuple) {
                        finalTuple.push(tuple);
                    });
                promises.push(feedQuery);
           }
           prom.all(promises).then(function(cumulativeResult) {
               var results = [];
               for(var j = 0; j < finalTuple.length; j++) {
                   for(var k = 0; k < finalTuple[j].length; k++) {
                       results.push(finalTuple[j][k]);
                   }
               }
               res.json(results);
           });
        });
    }

    function queryFeeds(req, res) {
        var userId = req.params.userId;
        feedModel.findAllFeedsForUser(userId)
            .then(
                function(resultant) {
                    var promises = [];
                    for(var i = 0; i < resultant.length; i++) {
                        var ps = api.articles({
                            source: resultant[i].value,
                            sortBy: 'top'
                        });
                        promises.push(ps);
                    }
                    prom.all(promises).then(function(finalResult) {
                        res.json(finalResult);
                    });
                }
            );
    }

    function findAllFeedsForUser(req, res) {
        var userId = req.params.userId;
        feedModel.findAllFeedsForUser(userId)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function findFeedById(req, res) {
        var feedId = req.params.feedId;
        feedModel.findFeedById(feedId)
            .then(
                function(feedData) {
                    res.json(feedData);
                }
            );
    }

    function deleteFeed(req, res) {
        var feedId = req.params.feedId;
        feedModel.deleteFeed(feedId)
            .then(
                function(feedDeletion) {
                    res.json(feedDeletion);
                }
            );
    }


};