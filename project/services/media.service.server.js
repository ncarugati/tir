/**
 * Created by Nicholas on 3/28/17.
 */
module.exports = function(app, models) {

    var profileModel = models.profileModel;

    app.post("/api/media/:userId/media", createMedia);
    app.get("/api/media/:userId/media", findAllMediaForProfile);
    app.get("/api/media/:mediaId", findMediaById);
    app.put("/api/media/:mediaId", updateMedia);
    app.delete("/api/media/:mediaId", deleteMedia);

    function createMedia(req, res) {
        var userId = req.params.userId;
        var media = req.body;
        media.userId = userId;

        profileModel.createMedia(userId, media)
            .then(
                function(feedCreation) {
                    res.json(feedCreation);
                }
            );
    }

    function findAllMediaForProfile(req, res) {
        var userId = req.params.userId;
        profileModel.findAllMediaForProfile(userId)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function findMediaById(req, res) {
        var mediaId = req.params.mediaId;
        profileModel.findMediaById(mediaId)
            .then(
                function(mediaQuery) {
                    res.json(mediaQuery);
                }
            );
    }

    function updateMedia(req, res) {
        var mediaId = req.params.mediaId;
        var media = req.body;
        profileModel.updateMedia(mediaId, media)
            .then(
                function(updatedMedia) {
                    res.json(updatedMedia);
                }
            );
    }

    function deleteMedia(req, res) {
        var mediaId = req.params.mediaId;
        profileModel.deleteMedia(mediaId)
            .then(
                function(mediaDeletion) {
                    res.json(mediaDeletion);
                }
            );
    }


};