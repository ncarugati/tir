/**
 * Created by Nicholas on 3/28/17.
 */
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;
var bcrypt = require("bcrypt-nodejs");


module.exports = function(app, models) {

    var modelUser = models.userModel;
    var multer = require('multer');
    var upload = multer({ dest: __dirname + '/../../public/uploads' });

    var facebookConfig = {
        clientID     : "198454067325809",
        clientSecret : "333a549f107bb84d3333332a1da5c53c",
        callbackURL  : "http://nicholas-carugati-webdev.herokuapp.com/auth/facebook/callback"
    };

    passport.serializeUser(serializeUser);
    passport.deserializeUser(deserializeUser);
    passport.use('wam', new LocalStrategy(localStrategy));
    passport.use(new FacebookStrategy(facebookConfig, facebookStrategy));

    //CRUD Operations
    app.post("/api/user", createUser);
    app.get("/api/user", findUser);
    app.get("/api/user/:userId", findUserById);
    app.put("/api/user/:userId", updateUser);
    app.delete("/api/user/:userId", deleteUser);

    //Follower Management
    app.put("/api/user/:userId/flw", follow);
    app.put("/api/user/:userId/unf", unfollow);
    app.put("/api/user/:userId/gf", gainFollower);
    app.put("/api/user/:userId/lf", loseFollower);
    app.get("/api/user/:userId/followers", findUserFollowers);
    app.get("/api/user/:userId/following", findUserFollowing);

    //Login Management
    app.post('/api/login', passport.authenticate('wam'), login);
    app.get('/auth/facebook', passport.authenticate('facebook', { scope : 'email' }));
    app.post('/api/logout', logout);
    app.post('/api/register', register);
    app.get('/api/loggedin', loggedIn);

    //Search management
    app.get('/api/search/:skey', searchForUser);

    //File upload
    app.post("/api/upload", upload.single('myFile'), uploadImage);

    //Custom callback provided by: http://passportjs.org/docs
    app.get('/auth/facebook/callback', function(req, res, next) {
        passport.authenticate('facebook', function(err, user, info) {
            if (err) {
                return next(err);
            }
            if (!user) {
                return res.redirect('/project/#/login');
            }
            req.logIn(user, function(err) {
                return res.redirect('/project/#/user/' + user._id);
            });
        })(req, res, next);
    });

    function loggedIn(req, res) {
        res.send(req.isAuthenticated() ? req.user : '0');
    }

    function createUser(req, res) {
        var user = req.body;
        user.password = bcrypt.hashSync(user.password);
        modelUser.createUser(user)
            .then(
                function(newUser) {
                    res.json(newUser);
                },
                function(error) {
                    console.log("ERROR: " + error);
                }
            );
    }

    function findUserById(req, res) {
        var id = req.params.userId;
        modelUser.findUserById(id)
            .then(
                function(userFound) {
                    res.json(userFound);
                }
            );
    }

    function findUser(req, res) {
        var query = req.query;
        if(query.password && query.username) {
            findUserByCredentials(query.username, query.password, res);
        } else if(query.username) {
            findUserByUsername(query.username, res);
        } else {
            res.sendStatus(400);
        }
    }



    function findUserByUsername(username, res) {
        modelUser.findUserByUsername(username)
            .then(
                function(userFound) {
                    res.json(userFound);
                }
            );
    }

    function findUserByCredentials(username, password, res) {
        modelUser.findUserByCredentials(username, password)
            .then(
                function(userFound) {
                    res.json(userFound);
                }
            );
    }

    function updateUser(req, res) {
        var id = req.params.userId;
        var userData = req.body;
        if(userData.password && req.query.password) {
            modelUser.findUserByUsername(userData.username)
                .then(function(user) {
                        if(user && bcrypt.compareSync(userData.password, user.password)) {
                            userData.password = bcrypt.hashSync(req.query.password);
                            updateUserData(res, id, userData);
                        } else {
                            res.sendStatus(401);
                        }
                    }
                );
        } else {
           updateUserData(res, id, userData);
        }
    }

    function updateUserData(res, id, user) {
        modelUser.updateUser(id, user)
            .then(
                function(userUpdate) {
                    res.json(userUpdate);
                }
            );
    }

    function deleteUser(req, res) {
        var id = req.params.userId;
        modelUser.deleteUser(id)
            .then(
                function(userDeletion) {
                    res.json(userDeletion);
                }
            );
    }

    function localStrategy(username, password, done) {
        modelUser.findUserByUsername(username)
            .then(
                function(user) {
                    if(user && bcrypt.compareSync(password, user.password)) {
                        return done(null, user);
                    } else {
                        return done(null, false);
                    }
                },
                function(err) {
                    if (err) { return done(err); }
                }
            );
    }

    function facebookStrategy(token, refreshToken, profile, done) {
        modelUser.findUserByFacebookId(profile.id)
            .then(
                function (facebookCredentials) {
                    if(facebookCredentials) {
                        return done(null, facebookCredentials);
                    } else {
                        facebookCredentials = {
                            username: profile.displayName.replace(/ /g, ''),
                            facebook: {
                                id:    profile.id,
                                token: token
                            }
                        };
                        modelUser.createUser(facebookCredentials)
                            .then(
                                function (user) {
                                    return done(null, user);
                                }
                            );
                    }
                }
            )
            .then(
                function(user) {
                    return done(null, user);
                }
            );
    }


    function serializeUser(user, done) {
        done(null, user);
    }


    function deserializeUser(user, done) {
        modelUser.findUserById(user._id)
            .then(
                function(user){
                    done(null, user);
                },
                function(err){
                    done(err, null);
                }
            );
    }

    function login(req, res) {
        var user = req.user;
        res.json(user);
    }

    function logout(req, res) {
        req.logOut();
        res.send(200);
    }

    function follow(req, res) {
        var user = req.body;
        var userId = req.params.userId;
        modelUser.follow(userId, user)
            .then(
                function(userUpdate) {
                    res.json(userUpdate);
                }
            );

    }

    function unfollow(req, res) {
        var user = req.body;
        var userId = req.params.userId;
        modelUser.unfollow(userId, user)
            .then(
                function(userUpdate) {
                    res.json(userUpdate);
                }
            );
    }

    function gainFollower(req, res) {
        var user = req.body;
        var userId = req.params.userId;
        modelUser.gainFollower(userId, user)
            .then(
                function(userUpdate) {
                    res.json(userUpdate);
                }
            );
    }

    function loseFollower(req, res) {
        var user = req.body;
        var userId = req.params.userId;
        modelUser.loseFollower(userId, user)
            .then(
                function(userUpdate) {
                    res.json(userUpdate);
                }
            );
    }

    function findUserFollowers(req, res) {
        var id = req.params.userId;
        modelUser.findUserFollowers(id)
            .then(
                function(userFollowers) {
                    res.json(userFollowers);
                }
            );
    }

    function findUserFollowing(req, res) {
        var id = req.params.userId;
        modelUser.findUserFollowing(id)
            .then(
                function(userFollowing) {
                    res.json(userFollowing);
                }
            );
    }

    function searchForUser(req, res) {
        var keyword = req.params.skey;
        modelUser.searchForUser(keyword)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }


    function register(req, res) {
        var user = req.body;
        modelUser.createUser(user)
            .then(function(user){
                    if(user){
                        req.login(user, function(err) {
                            if(err) {
                                console.log(err);
                                res.status(400).send(err);
                            } else {
                                res.json(user);
                            }
                        });
                    }
                }
            );
    }

    function uploadImage(req, res) {
        var width         = req.body.width;
        var myFile        = req.file;

        var urlPayload = "/project/#/user/edit/" + req.body.userId;

        if(!myFile) {
            res.redirect(urlPayload);
            return;
        }

        var originalname  = myFile.originalname;
        var filename      = myFile.filename;
        var path          = myFile.path;
        var destination   = myFile.destination;
        var size          = myFile.size;
        var mimetype      = myFile.mimetype;

        modelUser.uploadPicture(req.body.userId, "/uploads/" + filename)
            .then(
                function(userUpdate) {
                    res.json(userUpdate);
                }
            );
        res.redirect(urlPayload);

    }

};