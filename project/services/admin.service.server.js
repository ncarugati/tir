/**
 * Created by Nicholas on 4/9/17.
 */
module.exports = function(app, models) {

    var bcrypt = require("bcrypt-nodejs");

    var modelUser = models.userModel;
    var profileModel = models.profileModel;
    var commentModel = models.commentModel;


    app.post("/api/admin/user", createUser);
    app.get("/api/admin", getAllUsers);
    app.get("/api/admin/articles", getAllArticles);
    app.put("/api/admin", setRights);
    app.get("/api/admin/comments", getAllComments);


    function getAllUsers(req, res) {
        modelUser.getAllUsers()
            .then(function(response) {
                res.json(response);
            });
    }

    function getAllArticles(req, res) {
        profileModel.getAllMedia()
            .then(function(response) {
               res.json(response);
            });
    }

    function getAllComments(req, res) {
        commentModel.getAllComments()
            .then(function(response) {
                res.json(response);
            });
    }

    function setRights(req, res) {
        var rights = req.query.rights;
        var id = req.body._id;
        modelUser.setRights(id, rights)
            .then(function(response) {
                res.json(response);
            })
    }

    function createUser(req, res) {
        var user = req.body;
        user.password = bcrypt.hashSync(user.password);
        modelUser.createUser(user)
            .then(
                function(newUser) {
                    res.json(newUser);
                }
            );
    }


};