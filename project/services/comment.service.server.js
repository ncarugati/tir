/**
 * Created by Nicholas on 4/9/17.
 */
module.exports = function(app, models) {

    var commentModel = models.commentModel;

    app.post("/api/comment/:mediaId", createComment);
    app.get("/api/comment/user/:userId", findAllCommentsForUser);
    app.get("/api/comment/:mediaId", findAllCommentsForMedia);
    app.put("/api/comment/:commId", updateComment);
    app.delete("/api/comment/:commId", deleteComment);


    function findAllCommentsForUser(req, res) {
        var userId = req.params.userId;
        commentModel.findAllCommentsForUser(userId)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function findAllCommentsForMedia(req, res) {
        var mediaId = req.params.mediaId;

        commentModel.findAllCommentsForMedia(mediaId)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function updateComment(req, res) {
        var userId = req.params.commId;
        var text = req.body.text;
        commentModel.updateComment(userId, text)
            .then(
                function(resultant) {
                    res.json(resultant);
                }
            );
    }

    function deleteComment(req, res) {
        var commId = req.params.commId;
        commentModel.deleteComment(commId)
            .then(
                function(mediaDeletion) {
                    res.json(mediaDeletion);
                }
            );
    }

    function createComment(req, res) {
        var mediaId = req.params.mediaId;
        var comment = req.body;
        commentModel.createComment(mediaId, comment)
            .then(
                function(feedCreation) {
                    res.json(feedCreation);
                }
            );
    }


};