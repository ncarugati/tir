/**
 * Created by Nicholas on 3/28/17.
 */
module.exports = function(app) {

    var models = require("./model/models.server")();
    require("./services/user.service.server.js")(app, models);
    require("./services/media.service.server.js")(app, models);
    require("./services/feed.service.server.js")(app, models);
    require("./services/admin.service.server.js")(app, models);
    require("./services/comment.service.server.js")(app, models);
    require("./services/rating.service.server.js")(app, models);
};