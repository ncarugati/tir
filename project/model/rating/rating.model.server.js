/**
 * Created by Nicholas on 4/11/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");
    var RatingSchema = require("./rating.schema.server.js")(mongoose);
    var RatingModel = mongoose.model("ratingModel", RatingSchema);

    function createRating(mediaId, rating) {
        var query = {
            "_media": mediaId,
            "userId": rating.userId,
            "value": rating.value
        };
        return RatingModel.create(query);
    }

    function findRatingByUser(userId, mediaId) {
        return RatingModel.findOne({userId : userId, _media : mediaId});
    }

    function findAllRatingsForMedia(mediaId) {
        return RatingModel.find({_media: mediaId}).populate('_media').exec();
    }

    function updateRating(userId, mediaId, rate) {
        return RatingModel.findOneAndUpdate({userId : userId, _media : mediaId}, {$set: {value: rate}});
    }

    function deleteRating(userId, mediaId) {
        return RatingModel.findOneAndRemove({userId : userId, _media : mediaId});
    }

    var api = {
        createRating            : createRating,
        findRatingByUser        : findRatingByUser,
        findAllRatingsForMedia  : findAllRatingsForMedia,
        updateRating            : updateRating,
        deleteRating            : deleteRating
    };
    return api;


};