/**
 * Created by Nicholas on 4/11/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");

    var RatingSchema = mongoose.Schema({
        _media          : {type: mongoose.Schema.ObjectId, ref: 'profileModel'},
        userId          : String,
        value           : Boolean
    }, {collection  : 'ratingmodels'});
    return RatingSchema;
};