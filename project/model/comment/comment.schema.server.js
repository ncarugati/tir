/**
 * Created by Nicholas on 4/9/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");

    var CommentSchema = mongoose.Schema({
        _media          : {type: mongoose.Schema.ObjectId, ref: 'profileModel'},
        userId          : String,
        poster          : String,
        picture         : String,
        text            : String,
        date            : {type: Date, default: Date.now}
    }, {collection  : 'commentmodels'});
    return CommentSchema;
};