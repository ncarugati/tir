/**
 * Created by Nicholas on 4/9/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");
    var CommentSchema = require("./comment.schema.server.js")(mongoose);
    var CommentModel = mongoose.model("commentModel", CommentSchema);

    function createComment(mediaId, comment) {
        var query = {
            "_media": mediaId,
            "userId": comment.userId,
            "poster": comment.poster,
            "picture": comment.picture,
            "text": comment.text
        };
        return CommentModel.create(query);
    }

    function findAllCommentsForUser(userId) {
        return CommentModel.find({userId: userId}).exec();
    }

    function findAllCommentsForMedia(mediaId) {
        return CommentModel.find({_media: mediaId}).populate('_media').exec();
    }

    function updateComment(feedId, text) {
        return CommentModel.update({_id: feedId}, {$set: {text: text}});
    }

    function deleteComment(commentId) {
        return CommentModel.remove({_id: commentId});
    }

    function getAllComments() {
        return CommentModel.find({});
    }

    var api = {
        createComment           : createComment,
        findAllCommentsForUser  : findAllCommentsForUser,
        updateComment           : updateComment,
        deleteComment           : deleteComment,
        getAllComments          : getAllComments,
        findAllCommentsForMedia  : findAllCommentsForMedia
    };
    return api;


};