/**
 * Created by Nicholas on 3/28/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");
    var ProfileSchema = require("./media.schema.server.js")(mongoose);
    var ProfileModel = mongoose.model("profileModel", ProfileSchema);

    function createMedia(userId, media) {
        var query = {
            "_user"           : userId,
            "name"            : media.name,
            "poster"          : media.poster,
            "posterId"        : media.posterId,
            "author"          : media.author,
            "title"           : media.title,
            "description"     : media.description,
            "url"             : media.url,
            "image"           : media.image,
            "published"       : media.published,
            "postDate"        : media.postDate,
            "rating"          : 0
        };
        return ProfileModel.create(query);
    }

    function findAllMediaForProfile(userId) {
        return ProfileModel.find({_user: userId}).populate('_user').sort('published').exec();
    }

    function findMediaById(mediaId) {
        return ProfileModel.findOne({_id: mediaId});
    }

    function updateMedia(mediaId, media) {
        return ProfileModel.update({_id: mediaId}, {$set: media});
    }

    function deleteMedia(mediaId) {
        return ProfileModel.remove({_id: mediaId});
    }

    function getAllMedia() {
        return ProfileModel.find({});
    }

    var api = {
        createMedia             : createMedia,
        findAllMediaForProfile  : findAllMediaForProfile,
        findMediaById           : findMediaById,
        updateMedia             : updateMedia,
        deleteMedia             : deleteMedia,
        getAllMedia             : getAllMedia
    };
    return api;
};