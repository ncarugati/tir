/**
 * Created by Nicholas on 3/28/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");
    var CommentSchema = require("../comment/comment.schema.server.js")();
    var RatingSchema = require("../rating/rating.schema.server.js")();

    var ProfileSchema = mongoose.Schema({
        _user           : {type: mongoose.Schema.ObjectId, ref: 'modelUser'},
        poster          : String,
        posterId        : String,
        author          : String,
        title           : String,
        description     : String,
        url             : String,
        image           : String,
        published       : Date,
        postDate        : Date,
        rating          : Number,
        comments        : [CommentSchema],
        ratings         : [RatingSchema]
    }, {collection  : 'profilemodels'});
    return ProfileSchema;
};