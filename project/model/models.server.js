/**
 * Created by Nicholas on 3/28/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");
    var connectionString = 'mongodb://guestuser:guest@ds143900.mlab.com:43900/projectdev';

    if(process.env.MLAB_USERNAME) {
        connectionString = process.env.MLAB_USERNAME + ":" +
            process.env.MLAB_PASSWORD + "@" +
            process.env.MLAB_HOST + ':' +
            process.env.MLAB_PORT + '/' +
            process.env.MLAB_APP_NAME;
    }
    mongoose.connect(connectionString);

    var userModel = require("./user/user.model.server")();
    var profileModel = require("./media/media.model.server.js")();
    var feedModel = require("./feed/feed.model.server.js")();
    var commentModel = require("./comment/comment.model.server.js")();
    var ratingModel = require("./rating/rating.model.server.js")();

    var models = {
        userModel       : userModel,
        profileModel    : profileModel,
        feedModel       : feedModel,
        commentModel    : commentModel,
        ratingModel     : ratingModel
    };
    return models;
};