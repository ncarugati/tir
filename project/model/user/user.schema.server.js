/**
 * Created by Nicholas on 3/28/17.
 */

module.exports = function() {

    var mongoose = require("mongoose");
    var ProfileSchema = require("../media/media.schema.server.js")();
    var FeedSchema = require("../feed/feed.schema.server.js")();

    var UserSchema = mongoose.Schema({
        username: String,
        password: String,
        firstName: String,
        lastName: String,
        email: String,
        sharedArticles: [ProfileSchema],
        summary: String,
        gender: {type: String, enum:["Male", "Female", "Other"], default: 'Male'},
        phone: String,
        age: { type: Date },
        rights: Boolean,
        categories: [FeedSchema],
        picture: String,
        followers: [{
            username: String,
            uid: String,
            picture: String
        }],
        following: [{
            username: String,
            uid: String,
            picture: String
        }],
        facebook: {
            id:    String,
            token: String
        }
    }, { collection: 'usermodels' });

    return UserSchema;
};