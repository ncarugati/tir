/**
 * Created by Nicholas on 3/28/17.
 */

module.exports = function() {
    var mongoose = require("mongoose");
    var UserSchema = require("./user.schema.server")(mongoose);
    var UserModel = mongoose.model("modelUser", UserSchema);

    function createUser(user) {
        return UserModel.create(user);
    }

    function findUserById(userId) {
        return UserModel.findOne({_id: userId});
    }

    function findUserByUsername(username) {
        return UserModel.findOne({username: username});
    }

    function findUserByCredentials(username, password) {
        return UserModel.findOne({username: username, password: password});
    }

    function searchForUser(rgx) {
        return UserModel.find({ "username": { "$regex": rgx, "$options": "i" } });
    }

    function findUserFollowers(userId) {
        return UserModel.find({_id: userId}).select({"followers": 1});
    }

    function findUserFollowing(userId) {
        return UserModel.find({_id: userId}).select({"following": 1});
    }

    function follow(userId, user) {
        var config = {
            "username": user.username,
            "uid": user._id,
            "picture": user.picture
        };
        return UserModel.update({_id: userId}, {$push: {following: config}}, {safe: true, upsert: true});
    }

    function unfollow(userId, user) {
        var config = {
            "username": user.username,
            "uid": user._id,
            "picture": user.picture
        };
        return UserModel.update({_id: userId}, {$pull: {following: config}}, {safe: true, upsert: true});
    }

    function gainFollower(userId, user) {
        var config = {
            "username": user.username,
            "uid": user._id,
            "picture": user.picture
        };
        return UserModel.update({_id: userId}, {$push: {followers: config}}, {safe: true, upsert: true});
    }

    function loseFollower(userId, user) {
        var config = {
            "username": user.username,
            "uid": user._id,
            "picture": user.picture
        };
        return UserModel.update({_id: userId}, {$pull: {followers: config}}, {safe: true, upsert: true});
    }

    function updateUser(userId, user) {
        return UserModel.update(
            {_id: userId}, {
                username    : user.username,
                password    : user.password,
                firstName   : user.firstName,
                lastName    : user.lastName,
                email       : user.email,
                gender      : user.gender,
                phone       : user.phone,
                age         : user.age,
                summary     : user.summary
            })
    }

    function uploadPicture(userId, path) {
        return UserModel.update({_id: userId}, {$set: {picture: path}});
    }

    function deleteUser(userId) {
        return UserModel.remove({_id: userId});
    }

    function findUserByFacebookId(facebookId) {
        return UserModel.findOne({'facebook.id': facebookId});
    }

    function getAllUsers() {
        return UserModel.find({});
    }

    function setRights(userId, rights) {
        return UserModel.update({_id: userId}, {$set: {rights: rights}});
    }

    var api = {
        createUser              : createUser,
        findUserById            : findUserById,
        findUserByUsername      : findUserByUsername,
        findUserByCredentials   : findUserByCredentials,
        updateUser              : updateUser,
        deleteUser              : deleteUser,
        findUserByFacebookId    : findUserByFacebookId,
        follow                  : follow,
        unfollow                : unfollow,
        gainFollower            : gainFollower,
        loseFollower            : loseFollower,
        findUserFollowers       : findUserFollowers,
        findUserFollowing       : findUserFollowing,
        searchForUser           : searchForUser,
        uploadPicture           : uploadPicture,
        getAllUsers             : getAllUsers,
        setRights               : setRights
    };
    return api;

};