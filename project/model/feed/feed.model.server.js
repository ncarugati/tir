/**
 * Created by Nicholas on 3/28/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");
    var FeedSchema = require("./feed.schema.server.js")(mongoose);
    var FeedModel = mongoose.model("feedModel", FeedSchema);

    function createFeed(userId, feed) {
        var query = {
            "_user"         : userId,
            "name"          : feed.name,
            "value"         : feed.value,
            "type"          : feed.type
        };
        return FeedModel.create(query);
    }

    function findAllFeedsForUser(userId) {
        return FeedModel.find({_user: userId}).populate('_user').exec();
    }

    function findFeedById(feedId) {
        return FeedModel.findOne({_id: feedId});
    }

    function deleteFeed(feedId) {
        return FeedModel.remove({_id: feedId});
    }

    var api = {
        createFeed             : createFeed,
        findAllFeedsForUser    : findAllFeedsForUser,
        findFeedById           : findFeedById,
        deleteFeed             : deleteFeed
    };
    return api;
};