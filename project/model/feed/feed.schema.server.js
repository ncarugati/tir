/**
 * Created by Nicholas on 3/28/17.
 */
module.exports = function() {

    var mongoose = require("mongoose");

    var FeedSchema = mongoose.Schema({
        _user       : {type: mongoose.Schema.ObjectId, ref: 'modelUser'},
        name        : String,
        value       : String,
        type: {type: String, enum:["SOURCE", "CATEGORY"], default: "SOURCE"}
    }, {collection  : 'feedmodels'});
    return FeedSchema;
};